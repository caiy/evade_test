#!/usr/bin/env python3
import glob
import matplotlib.pyplot as plt

def draw2DScatterColored(input_data_x, input_data_y, input_color, xname, yname, title, size = 3 , outfile=""):
    fig, ax = plt.subplots()
    sc = ax.scatter(input_data_x, input_data_y, c=input_color, s=size, edgecolors='none')

    ax.legend()
    ax.grid(True)
    
    ax.set_xlabel(xname)
    ax.set_ylabel(yname)
    fig.suptitle(title)
    plt.colorbar(sc)
    # Show the canvas
    if outfile == "":
        outname = str(title+"_"+xname+"_"+yname)
    else:
        outname = outfile
    plt.savefig(outname+".pdf")


if __name__ == '__main__':
    
    import pandas as pd
    df_in = pd.read_csv('./localrun.in',  sep='\t',)
    print(df_in)
    df_out = pd.read_csv('./localrun.tsv',  sep='\t',)
    print(df_out)
    df_aux = pd.read_csv('./aux.save',  sep='\t',)
    print(df_aux)
    df = pd.concat([df_in, df_out, df_aux], axis=1)
    print(df)
    df['deep_B'] = df['deep_B'].apply(lambda x: x if x <=500 else 500 )
    df['fast_B'] = df['fast_B'].apply(lambda x: x if x <=500 else 500 )
    df['deep_B'] = df['deep_B'].apply(lambda x: x if x >=300 else 300 )
    df['fast_B'] = df['fast_B'].apply(lambda x: x if x >=300 else 300 )
    draw2DScatterColored( df["mA"] , df["tanb"] , df['fast_B'], xname = "mA", yname = "tan(beta)", title = "fast B", size = 3 , outfile="./EVADE_fastB_mA_tanBeta" )
    draw2DScatterColored( df["mA"] , df["tanb"] , df['deep_B'], xname = "mA", yname = "tan(beta)", title = "deep B", size = 3 , outfile="./EVADE_deepB_mA_tanBeta" )

