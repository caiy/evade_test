# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 13.10.2023,  16:22
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.51526472E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    1.50032529E+03  # scale for input parameters
    1    1.00000000E+03  # M_1
    2    1.00000000E+03  # M_2
    3    2.50000000E+03  # M_3
   11    2.82941176E+03  # A_t
   12    2.82941176E+03  # A_b
   13    2.82941176E+03  # A_tau
   23    1.00000000E+03  # mu
   25    3.40000000E+01  # tan(beta)
   26    1.85000000E+03  # m_A, pole mass
   31    2.00000000E+03  # M_L11
   32    2.00000000E+03  # M_L22
   33    2.00000000E+03  # M_L33
   34    2.00000000E+03  # M_E11
   35    2.00000000E+03  # M_E22
   36    2.00000000E+03  # M_E33
   41    1.50000000E+03  # M_Q11
   42    1.50000000E+03  # M_Q22
   43    1.50000000E+03  # M_Q33
   44    1.50000000E+03  # M_U11
   45    1.50000000E+03  # M_U22
   46    1.50000000E+03  # M_U33
   47    1.50000000E+03  # M_D11
   48    1.50000000E+03  # M_D22
   49    1.50000000E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  1.50032529E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  1.50032529E+03  # (SUSY scale)
  1  1     8.38799781E-06   # Y_u(Q)^DRbar
  2  2     4.26110289E-03   # Y_c(Q)^DRbar
  3  3     1.01333751E+00   # Y_t(Q)^DRbar
Block Yd Q=  1.50032529E+03  # (SUSY scale)
  1  1     5.73082930E-04   # Y_d(Q)^DRbar
  2  2     1.08885757E-02   # Y_s(Q)^DRbar
  3  3     5.68318689E-01   # Y_b(Q)^DRbar
Block Ye Q=  1.50032529E+03  # (SUSY scale)
  1  1     1.00007162E-04   # Y_e(Q)^DRbar
  2  2     2.06783093E-02   # Y_mu(Q)^DRbar
  3  3     3.47775145E-01   # Y_tau(Q)^DRbar
Block Au Q=  1.50032529E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.82941176E+03   # A_t(Q)^DRbar
Block Ad Q=  1.50032529E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     2.82941132E+03   # A_b(Q)^DRbar
Block Ae Q=  1.50032529E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     2.82941179E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  1.50032529E+03  # soft SUSY breaking masses at Q
   1    1.00000000E+03  # M_1
   2    1.00000000E+03  # M_2
   3    2.50000000E+03  # M_3
  21    2.43059645E+06  # M^2_(H,d)
  22   -9.57044849E+05  # M^2_(H,u)
  31    2.00000000E+03  # M_(L,11)
  32    2.00000000E+03  # M_(L,22)
  33    2.00000000E+03  # M_(L,33)
  34    2.00000000E+03  # M_(E,11)
  35    2.00000000E+03  # M_(E,22)
  36    2.00000000E+03  # M_(E,33)
  41    1.50000000E+03  # M_(Q,11)
  42    1.50000000E+03  # M_(Q,22)
  43    1.50000000E+03  # M_(Q,33)
  44    1.50000000E+03  # M_(U,11)
  45    1.50000000E+03  # M_(U,22)
  46    1.50000000E+03  # M_(U,33)
  47    1.50000000E+03  # M_(D,11)
  48    1.50000000E+03  # M_(D,22)
  49    1.50000000E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22822845E+02  # h0
        35     1.85017555E+03  # H0
        36     1.85000000E+03  # A0
        37     1.85243401E+03  # H+
   1000001     1.55986035E+03  # ~d_L
   2000001     1.55179438E+03  # ~d_R
   1000002     1.55786267E+03  # ~u_L
   2000002     1.55171258E+03  # ~u_R
   1000003     1.55988366E+03  # ~s_L
   2000003     1.55176778E+03  # ~s_R
   1000004     1.55786170E+03  # ~c_L
   2000004     1.55171243E+03  # ~c_R
   1000005     1.51449719E+03  # ~b_1
   2000005     1.55701292E+03  # ~b_2
   1000006     1.37357769E+03  # ~t_1
   2000006     1.63876859E+03  # ~t_2
   1000011     2.00568877E+03  # ~e_L-
   2000011     2.00315206E+03  # ~e_R-
   1000012     2.00376380E+03  # ~nu_eL
   1000013     2.00597203E+03  # ~mu_L-
   2000013     2.00284912E+03  # ~mu_R-
   1000014     2.00375735E+03  # ~nu_muL
   1000015     1.98825789E+03  # ~tau_1-
   2000015     2.01648582E+03  # ~tau_2-
   1000016     2.00242199E+03  # ~nu_tauL
   1000021     2.43547752E+03  # ~g
   1000022     9.46061057E+02  # ~chi_10
   1000023     1.00253814E+03  # ~chi_20
   1000025     1.00744755E+03  # ~chi_30
   1000035     1.07860288E+03  # ~chi_40
   1000024     9.59738719E+02  # ~chi_1+
   1000037     1.07332805E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.86840946E-02   # alpha
Block Hmix Q=  1.50032529E+03  # Higgs mixing parameters
   1    1.00000000E+03  # mu
   2    3.40000000E+01  # tan[beta](Q)
   3    2.44031503E+02  # v(Q)
   4    3.42250000E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -6.69084127E-01   # Re[R_st(1,1)]
   1  2     7.43186673E-01   # Re[R_st(1,2)]
   2  1    -7.43186673E-01   # Re[R_st(2,1)]
   2  2    -6.69084127E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     8.87676861E-01   # Re[R_sb(1,1)]
   1  2     4.60466927E-01   # Re[R_sb(1,2)]
   2  1    -4.60466927E-01   # Re[R_sb(2,1)]
   2  2     8.87676861E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     6.59909125E-01   # Re[R_sta(1,1)]
   1  2     7.51345424E-01   # Re[R_sta(1,2)]
   2  1    -7.51345424E-01   # Re[R_sta(2,1)]
   2  2     6.59909125E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     4.59438227E-01   # Re[N(1,1)]
   1  2    -5.12776715E-01   # Re[N(1,2)]
   1  3     5.27651974E-01   # Re[N(1,3)]
   1  4    -4.97553967E-01   # Re[N(1,4)]
   2  1     8.44908345E-01   # Re[N(2,1)]
   2  2     5.04118659E-01   # Re[N(2,2)]
   2  3    -1.27018495E-01   # Re[N(2,3)]
   2  4     1.25938745E-01   # Re[N(2,4)]
   3  1     1.52343988E-02   # Re[N(3,1)]
   3  2    -2.67827303E-02   # Re[N(3,2)]
   3  3    -7.06244540E-01   # Re[N(3,3)]
   3  4    -7.07297143E-01   # Re[N(3,4)]
   4  1    -2.73522059E-01   # Re[N(4,1)]
   4  2     6.94411336E-01   # Re[N(4,2)]
   4  3     4.54607904E-01   # Re[N(4,3)]
   4  4    -4.86117510E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -6.31583652E-01   # Re[U(1,1)]
   1  2     7.75307739E-01   # Re[U(1,2)]
   2  1     7.75307739E-01   # Re[U(2,1)]
   2  2     6.31583652E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -6.72159652E-01   # Re[V(1,1)]
   1  2     7.40406241E-01   # Re[V(1,2)]
   2  1     7.40406241E-01   # Re[V(2,1)]
   2  2     6.72159652E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.69994812E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     2.25000344E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.08161886E-01    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.28742251E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     6.66089883E-02    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.58511982E+01   # ~e^-_L
#    BR                NDA      ID1      ID2
     2.40618450E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.86122959E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.08236731E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     8.45906263E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     2.64142753E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     3.40973580E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.71701829E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     2.24755039E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     7.05825518E-01    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.05743443E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     6.66948357E-02    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     1.06315645E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     6.04016555E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.58661568E+01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     2.42272159E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.85956885E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     4.08147203E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     8.46550482E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     2.63975259E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     3.40777445E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.47021351E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.18020068E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.88799725E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     8.37131124E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.54165894E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.64411941E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     4.15134943E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.48851765E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.50442257E-03    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.95065670E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     9.78394352E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.82568084E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.05853139E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.92436799E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.58739289E+01   # ~nu_e
#    BR                NDA      ID1      ID2
     1.92879989E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     8.68905480E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.78299602E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     1.98011904E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.98116836E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     3.09744065E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.58833457E+01   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.92764303E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     8.68383797E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.78072453E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     1.97892831E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.98319627E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     3.09776782E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.85425701E+01   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.64883290E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     7.42689350E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.23344467E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.69215325E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.47171754E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     3.17663572E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     2.99520693E-01   # ~d_R
#    BR                NDA      ID1      ID2
     2.41040757E-01    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     7.00901321E-01    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     2.30342210E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     5.78161480E-02    2     1000035         1   # BR(~d_R -> chi^0_4 d)
DECAY   1000001     6.70404621E+00   # ~d_L
#    BR                NDA      ID1      ID2
     1.44688140E-01    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.41259293E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     3.02956816E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     1.54021888E-01    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     3.15531106E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     3.41329980E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
DECAY   2000003     3.01965655E-01   # ~s_R
#    BR                NDA      ID1      ID2
     2.40393672E-01    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     6.95250731E-01    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     2.24663141E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     5.80156028E-02    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     2.77802131E-03    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
     1.31534135E-03    2    -1000037         4   # BR(~s_R -> chi^-_2 c)
DECAY   1000003     6.70587612E+00   # ~s_L
#    BR                NDA      ID1      ID2
     1.44715056E-01    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.41192066E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     3.95719986E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.54023059E-01    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     3.15469193E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     3.41277766E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
DECAY   1000005     1.72536853E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.55108663E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.80395586E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     8.41404776E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     3.41874085E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.39112645E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.20128777E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     1.49282471E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     1.18231405E+01   # ~b_2
#    BR                NDA      ID1      ID2
     2.48638800E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.27827684E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.48189474E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.26668798E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     1.78380350E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     3.36090297E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.63024433E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     1.19781570E+00   # ~u_R
#    BR                NDA      ID1      ID2
     2.41048342E-01    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     7.00914416E-01    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     2.24680259E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     5.78125613E-02    2     1000035         2   # BR(~u_R -> chi^0_4 u)
DECAY   1000002     6.75207237E+00   # ~u_L
#    BR                NDA      ID1      ID2
     7.51487520E-02    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.49350374E-01    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.99153138E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.14770975E-01    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.53272645E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     3.07258102E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
DECAY   2000004     1.19818202E+00   # ~c_R
#    BR                NDA      ID1      ID2
     2.41007579E-01    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     7.00704359E-01    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.02382327E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     5.78304189E-02    2     1000035         4   # BR(~c_R -> chi^0_4 c)
DECAY   1000004     6.75347956E+00   # ~c_L
#    BR                NDA      ID1      ID2
     7.51381138E-02    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.49317612E-01    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     2.13006681E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.14755144E-01    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     3.53319947E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     3.07256177E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
DECAY   1000006     1.15539321E+01   # ~t_1
#    BR                NDA      ID1      ID2
     5.98480484E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     8.27214449E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.09123010E-02    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     3.21101644E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     3.03825977E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.95369267E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     9.60258346E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.72950602E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     3.45567543E+01   # ~t_2
#    BR                NDA      ID1      ID2
     1.51155909E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     4.18625688E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.42455400E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.47589350E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.26086513E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.42762272E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.18504510E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     2.33146799E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     4.35333671E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     9.79822298E-07   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.41159614E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.27060629E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.13587179E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.13553624E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.04638954E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     3.88284776E-01   # chi^+_2
#    BR                NDA      ID1      ID2
     5.28234016E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     4.52431786E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
#    BR                NDA      ID1      ID2       ID3
     1.66736765E-03    3     1000023        -1         2   # BR(chi^+_2 -> chi^0_2 d_bar u)
     1.66583738E-03    3     1000023        -3         4   # BR(chi^+_2 -> chi^0_2 s_bar c)
     5.54868844E-04    3     1000023       -11        12   # BR(chi^+_2 -> chi^0_2 e^+ nu_e)
     5.54865858E-04    3     1000023       -13        14   # BR(chi^+_2 -> chi^0_2 mu^+ nu_mu)
     5.53281828E-04    3     1000023       -15        16   # BR(chi^+_2 -> chi^0_2 tau^+ nu_tau)
     4.78087249E-03    3     1000025        -1         2   # BR(chi^+_2 -> chi^0_3 d_bar u)
     4.77549610E-03    3     1000025        -3         4   # BR(chi^+_2 -> chi^0_3 s_bar c)
     1.59355336E-03    3     1000025       -11        12   # BR(chi^+_2 -> chi^0_3 e^+ nu_e)
     1.59354578E-03    3     1000025       -13        14   # BR(chi^+_2 -> chi^0_3 mu^+ nu_mu)
     1.59068623E-03    3     1000025       -15        16   # BR(chi^+_2 -> chi^0_3 tau^+ nu_tau)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.88118360E-04   # chi^0_2
#    BR                NDA      ID1      ID2
     1.21008822E-04    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.67094618E-01    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     1.67094618E-01    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     1.66444905E-01    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     1.66444905E-01    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     5.55884738E-02    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     5.55884738E-02    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     5.55881296E-02    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     5.55881296E-02    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     5.51103888E-02    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     5.51103888E-02    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     4.26034096E-03   # chi^0_3
#    BR                NDA      ID1      ID2
     1.38575491E-02    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     7.05967560E-02    3     1000022         2        -2   # BR(chi^0_3 -> chi^0_1 u u_bar)
     7.02756995E-02    3     1000022         4        -4   # BR(chi^0_3 -> chi^0_1 c c_bar)
     9.04909773E-02    3     1000022         1        -1   # BR(chi^0_3 -> chi^0_1 d d_bar)
     9.04892813E-02    3     1000022         3        -3   # BR(chi^0_3 -> chi^0_1 s s_bar)
     8.74576620E-02    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     2.04238465E-02    3     1000022        11       -11   # BR(chi^0_3 -> chi^0_1 e^- e^+)
     2.04232581E-02    3     1000022        13       -13   # BR(chi^0_3 -> chi^0_1 mu^- mu^+)
     2.02589552E-02    3     1000022        15       -15   # BR(chi^0_3 -> chi^0_1 tau^- tau^+)
     1.21048384E-01    3     1000022        12       -12   # BR(chi^0_3 -> chi^0_1 nu_e nu_bar_e)
     6.58762985E-02    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     6.58762985E-02    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     6.56898669E-02    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     6.56898669E-02    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     2.19595840E-02    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     2.19595840E-02    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     2.19593192E-02    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     2.19593192E-02    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     2.18517497E-02    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     2.18517497E-02    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     5.11355136E-01   # chi^0_4
#    BR                NDA      ID1      ID2
     4.92174752E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     4.92174752E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.87840125E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.38204166E-03    3     1000025         2        -2   # BR(chi^0_4 -> chi^0_3 u u_bar)
     1.37781809E-03    3     1000025         4        -4   # BR(chi^0_4 -> chi^0_3 c c_bar)
     1.77278555E-03    3     1000025         1        -1   # BR(chi^0_4 -> chi^0_3 d d_bar)
     1.77276680E-03    3     1000025         3        -3   # BR(chi^0_4 -> chi^0_3 s s_bar)
     1.73532528E-03    3     1000025         5        -5   # BR(chi^0_4 -> chi^0_3 b b_bar)
     4.00001308E-04    3     1000025        11       -11   # BR(chi^0_4 -> chi^0_3 e^- e^+)
     3.99994500E-04    3     1000025        13       -13   # BR(chi^0_4 -> chi^0_3 mu^- mu^+)
     3.97894433E-04    3     1000025        15       -15   # BR(chi^0_4 -> chi^0_3 tau^- tau^+)
     2.36988542E-03    3     1000025        12       -12   # BR(chi^0_4 -> chi^0_3 nu_e nu_bar_e)
DECAY   1000021     3.17989239E+02   # ~g
#    BR                NDA      ID1      ID2
     4.00040846E-02    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     4.00040846E-02    2    -2000002         2   # BR(~g -> ~u^*_R u)
     3.95710396E-02    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     3.95710396E-02    2    -1000002         2   # BR(~g -> ~u^*_L u)
     4.00040702E-02    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     4.00040702E-02    2    -2000004         4   # BR(~g -> ~c^*_R c)
     3.95710826E-02    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     3.95710826E-02    2    -1000004         4   # BR(~g -> ~c^*_L c)
     1.17492863E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.17492863E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     6.31124781E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     6.31124781E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.48535452E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     2.48535452E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     3.99983207E-02    2     2000001        -1   # BR(~g -> ~d_R d_bar)
     3.99983207E-02    2    -2000001         1   # BR(~g -> ~d^*_R d)
     3.94305175E-02    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     3.94305175E-02    2    -1000001         1   # BR(~g -> ~d^*_L d)
     4.00001949E-02    2     2000003        -3   # BR(~g -> ~s_R s_bar)
     4.00001949E-02    2    -2000003         3   # BR(~g -> ~s^*_R s)
     3.94288781E-02    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     3.94288781E-02    2    -1000003         3   # BR(~g -> ~s^*_L s)
     4.24416250E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     4.24416250E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     3.98186375E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     3.98186375E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
DECAY        25     4.16533793E-03   # h^0
#    BR                NDA      ID1      ID2
     2.09160830E-04    2          13       -13   # BR(h^0 -> mu^- mu^+)
     5.84161096E-02    2          15       -15   # BR(h^0 -> tau^- tau^+)
     2.52516771E-04    2           3        -3   # BR(h^0 -> s s_bar)
     6.41462724E-01    2           5        -5   # BR(h^0 -> b b_bar)
     4.40543343E-02    2           4        -4   # BR(h^0 -> c c_bar)
     8.78199301E-02    2          21        21   # BR(h^0 -> g g)
     2.48389829E-03    2          22        22   # BR(h^0 -> photon photon)
# writing decays into V V* as 3-body decays
#    BR                NDA      ID1      ID2       ID3
     7.50074049E-03    3          24        11        12   # BR(h^0 -> W^+ e^- nu_e)
     7.50074049E-03    3          24        13        14   # BR(h^0 -> W^+ mu^- nu_mu)
     7.50074049E-03    3          24        15        16   # BR(h^0 -> W^+ tau^- nu_tau)
     2.62525917E-02    3          24         1        -2   # BR(h^0 -> W^+ d u_bar)
     2.62525917E-02    3          24         3        -4   # BR(h^0 -> W^+ s c_bar)
     7.50074049E-03    3         -24       -11       -12   # BR(h^0 -> W^- e^+ nu_bar_e)
     7.50074049E-03    3         -24       -13       -14   # BR(h^0 -> W^- mu^+ nu_bar_mu)
     7.50074049E-03    3         -24       -15       -16   # BR(h^0 -> W^- tau^+ nu_bar_tau)
     2.62525917E-02    3         -24        -1         2   # BR(h^0 -> W^- d_bar u)
     2.62525917E-02    3         -24        -3         4   # BR(h^0 -> W^- s_bar c)
     5.13597557E-04    3          23        11       -11   # BR(h^0 -> Z e^- e^+)
     5.13597557E-04    3          23        13       -13   # BR(h^0 -> Z mu^- mu^+)
     5.16654685E-04    3          23        15       -15   # BR(h^0 -> Z tau^- tau^+)
     3.05712832E-03    3          23        12       -12   # BR(h^0 -> Z nu_e nu_bar_e)
     2.38456009E-03    3          23         1        -1   # BR(h^0 -> Z d d_bar)
     2.38456009E-03    3          23         3        -3   # BR(h^0 -> Z s s_bar)
     2.30813188E-03    3          23         5        -5   # BR(h^0 -> Z b b_bar)
     1.77313442E-03    3          23         2        -2   # BR(h^0 -> Z u u_bar)
     1.83427699E-03    3          23         4        -4   # BR(h^0 -> Z c c_bar)
DECAY        35     4.54443949E+01   # H^0
#    BR                NDA      ID1      ID2
     3.46047269E-04    2          13       -13   # BR(H^0 -> mu^- mu^+)
     9.78813741E-02    2          15       -15   # BR(H^0 -> tau^- tau^+)
     3.30038361E-04    2           3        -3   # BR(H^0 -> s s_bar)
     8.99099955E-01    2           5        -5   # BR(H^0 -> b b_bar)
     2.29085236E-03    2           6        -6   # BR(H^0 -> t t_bar)
DECAY        36     4.54450222E+01   # A^0
#    BR                NDA      ID1      ID2
     3.45995205E-04    2          13       -13   # BR(A^0 -> mu^- mu^+)
     9.78670071E-02    2          15       -15   # BR(A^0 -> tau^- tau^+)
     3.29988333E-04    2           3        -3   # BR(A^0 -> s s_bar)
     8.98962971E-01    2           5        -5   # BR(A^0 -> b b_bar)
     2.45944289E-03    2           6        -6   # BR(A^0 -> t t_bar)
DECAY        37     3.96349761E+01   # H^+
#    BR                NDA      ID1      ID2
     3.97236198E-04    2         -13        12   # BR(H^+ -> mu^+ nu_e)
     1.12360858E-01    2         -15        12   # BR(H^+ -> tau^+ nu_e)
     3.30475514E-04    2          -3         4   # BR(H^+ -> s_bar c)
     8.86882814E-01    2          -5         6   # BR(H^+ -> b_bar t)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.51692263E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.15604831E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.15600000E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00004179E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    8.23263203E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    8.65051903E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.51692263E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.15604831E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.15600000E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999483E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    5.17239351E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999483E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    5.17239351E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25312668E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    4.66101005E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.21817861E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    5.17239351E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999483E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.59809709E-04   # BR(b -> s gamma)
    2    1.58475575E-06   # BR(b -> s mu+ mu-)
    3    3.51934202E-05   # BR(b -> s nu nu)
    4    3.03864380E-15   # BR(Bd -> e+ e-)
    5    1.29807272E-10   # BR(Bd -> mu+ mu-)
    6    2.71657323E-08   # BR(Bd -> tau+ tau-)
    7    1.01564854E-13   # BR(Bs -> e+ e-)
    8    4.33884107E-09   # BR(Bs -> mu+ mu-)
    9    9.19939539E-07   # BR(Bs -> tau+ tau-)
   10    9.39415996E-05   # BR(B_u -> tau nu)
   11    9.70380759E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.43679648E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94140349E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16760558E-03   # epsilon_K
   17    2.28176607E-15   # Delta(M_K)
   18    2.47688126E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28120159E-11   # BR(K^+ -> pi^+ nu nu)
   20    4.62451287E-15   # Delta(g-2)_electron/2
   21    1.97711106E-10   # Delta(g-2)_muon/2
   22    5.58892934E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    8.55542277E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.32235352E-01   # C7
     0305 4322   00   2    -8.69574973E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.28005135E-01   # C8
     0305 6321   00   2    -6.51520168E-04   # C8'
 03051111 4133   00   0     1.59215389E+00   # C9 e+e-
 03051111 4133   00   2     1.59285183E+00   # C9 e+e-
 03051111 4233   00   2     8.31726859E-05   # C9' e+e-
 03051111 4137   00   0    -4.41484600E+00   # C10 e+e-
 03051111 4137   00   2    -4.40738757E+00   # C10 e+e-
 03051111 4237   00   2    -6.46438382E-04   # C10' e+e-
 03051313 4133   00   0     1.59215389E+00   # C9 mu+mu-
 03051313 4133   00   2     1.59285020E+00   # C9 mu+mu-
 03051313 4233   00   2     8.31713919E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.41484600E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.40738920E+00   # C10 mu+mu-
 03051313 4237   00   2    -6.46438441E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50373036E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.40878058E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50373036E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.40878261E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50373036E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.40935336E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85707102E-07   # C7
     0305 4422   00   2     3.24806747E-05   # C7
     0305 4322   00   2    -1.09452388E-05   # C7'
     0305 6421   00   0     3.30383641E-07   # C8
     0305 6421   00   2     9.59345813E-07   # C8
     0305 6321   00   2    -5.79937635E-06   # C8'
 03051111 4133   00   2    -3.33226122E-06   # C9 e+e-
 03051111 4233   00   2     2.58550878E-06   # C9' e+e-
 03051111 4137   00   2     2.88463171E-05   # C10 e+e-
 03051111 4237   00   2    -2.01169547E-05   # C10' e+e-
 03051313 4133   00   2    -3.33223039E-06   # C9 mu+mu-
 03051313 4233   00   2     2.58550304E-06   # C9' mu+mu-
 03051313 4137   00   2     2.88463626E-05   # C10 mu+mu-
 03051313 4237   00   2    -2.01169731E-05   # C10' mu+mu-
 03051212 4137   00   2    -6.17197555E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     4.38407664E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -6.17197564E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     4.38407664E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -6.17202185E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     4.38407665E-06   # C11' nu_3 nu_3
