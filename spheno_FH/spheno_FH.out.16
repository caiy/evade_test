# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 13.10.2023,  16:06
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.08372628E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    1.42484158E+03  # scale for input parameters
    1    1.00000000E+03  # M_1
    2    1.00000000E+03  # M_2
    3    2.50000000E+03  # M_3
   11    3.80000000E+03  # A_t
   12    3.80000000E+03  # A_b
   13    3.80000000E+03  # A_tau
   23    1.00000000E+03  # mu
   25    1.00000000E+00  # tan(beta)
   26    1.75000000E+03  # m_A, pole mass
   31    2.00000000E+03  # M_L11
   32    2.00000000E+03  # M_L22
   33    2.00000000E+03  # M_L33
   34    2.00000000E+03  # M_E11
   35    2.00000000E+03  # M_E22
   36    2.00000000E+03  # M_E33
   41    1.50000000E+03  # M_Q11
   42    1.50000000E+03  # M_Q22
   43    1.50000000E+03  # M_Q33
   44    1.50000000E+03  # M_U11
   45    1.50000000E+03  # M_U22
   46    1.50000000E+03  # M_U33
   47    1.50000000E+03  # M_D11
   48    1.50000000E+03  # M_D22
   49    1.50000000E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  1.42484158E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  1.42484158E+03  # (SUSY scale)
  1  1     1.18572928E-05   # Y_u(Q)^DRbar
  2  2     6.02350473E-03   # Y_c(Q)^DRbar
  3  3     1.43245621E+00   # Y_t(Q)^DRbar
Block Yd Q=  1.42484158E+03  # (SUSY scale)
  1  1     2.38268039E-05   # Y_d(Q)^DRbar
  2  2     4.52709275E-04   # Y_s(Q)^DRbar
  3  3     2.36287233E-02   # Y_b(Q)^DRbar
Block Ye Q=  1.42484158E+03  # (SUSY scale)
  1  1     4.15795152E-06   # Y_e(Q)^DRbar
  2  2     8.59732502E-04   # Y_mu(Q)^DRbar
  3  3     1.44592864E-02   # Y_tau(Q)^DRbar
Block Au Q=  1.42484158E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     3.80000001E+03   # A_t(Q)^DRbar
Block Ad Q=  1.42484158E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     3.79999998E+03   # A_b(Q)^DRbar
Block Ae Q=  1.42484158E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     3.80000000E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  1.42484158E+03  # soft SUSY breaking masses at Q
   1    1.00000000E+03  # M_1
   2    1.00000000E+03  # M_2
   3    2.50000000E+03  # M_3
  21    5.78201191E+05  # M^2_(H,d)
  22    7.14186475E+05  # M^2_(H,u)
  31    2.00000000E+03  # M_(L,11)
  32    2.00000000E+03  # M_(L,22)
  33    2.00000000E+03  # M_(L,33)
  34    2.00000000E+03  # M_(E,11)
  35    2.00000000E+03  # M_(E,22)
  36    2.00000000E+03  # M_(E,33)
  41    1.50000000E+03  # M_(Q,11)
  42    1.50000000E+03  # M_(Q,22)
  43    1.50000000E+03  # M_(Q,33)
  44    1.50000000E+03  # M_(U,11)
  45    1.50000000E+03  # M_(U,22)
  46    1.50000000E+03  # M_(U,33)
  47    1.50000000E+03  # M_(D,11)
  48    1.50000000E+03  # M_(D,22)
  49    1.50000000E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     8.97913198E+01  # h0
        35     1.75126915E+03  # H0
        36     1.75000000E+03  # A0
        37     1.75250737E+03  # H+
   1000001     1.55838483E+03  # ~d_L
   2000001     1.55121591E+03  # ~d_R
   1000002     1.55860076E+03  # ~u_L
   2000002     1.55240017E+03  # ~u_R
   1000003     1.55838442E+03  # ~s_L
   2000003     1.55121587E+03  # ~s_R
   1000004     1.55860837E+03  # ~c_L
   2000004     1.55239144E+03  # ~c_R
   1000005     1.46988466E+03  # ~b_1
   2000005     1.55122457E+03  # ~b_2
   1000006     1.29660295E+03  # ~t_1
   2000006     1.56576346E+03  # ~t_2
   1000011     2.00535347E+03  # ~e_L-
   2000011     2.00222955E+03  # ~e_R-
   1000012     2.00502385E+03  # ~nu_eL
   1000013     2.00535369E+03  # ~mu_L-
   2000013     2.00222930E+03  # ~mu_R-
   1000014     2.00502384E+03  # ~nu_muL
   1000015     2.00178920E+03  # ~tau_1-
   2000015     2.00577486E+03  # ~tau_2-
   1000016     2.00501858E+03  # ~nu_tauL
   1000021     2.43556074E+03  # ~g
   1000022     9.05335514E+02  # ~chi_10
   1000023     9.77449304E+02  # ~chi_20
   1000025     1.00086229E+03  # ~chi_30
   1000035     1.09276900E+03  # ~chi_40
   1000024     9.18661556E+02  # ~chi_1+
   1000037     1.08404790E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -7.47630206E-01   # alpha
Block Hmix Q=  1.42484158E+03  # Higgs mixing parameters
   1    1.00000000E+03  # mu
   2    1.00000000E+00  # tan[beta](Q)
   3    2.44125359E+02  # v(Q)
   4    3.06250000E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -6.08443629E-01   # Re[R_st(1,1)]
   1  2     7.93597096E-01   # Re[R_st(1,2)]
   2  1    -7.93597096E-01   # Re[R_st(2,1)]
   2  2    -6.08443629E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99679159E-01   # Re[R_sb(1,1)]
   1  2     2.53294200E-02   # Re[R_sb(1,2)]
   2  1    -2.53294200E-02   # Re[R_sb(2,1)]
   2  2    -9.99679159E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -3.28725967E-01   # Re[R_sta(1,1)]
   1  2     9.44425348E-01   # Re[R_sta(1,2)]
   2  1    -9.44425348E-01   # Re[R_sta(2,1)]
   2  2    -3.28725967E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     3.72934367E-01   # Re[N(1,1)]
   1  2    -5.25397372E-01   # Re[N(1,2)]
   1  3     5.40704765E-01   # Re[N(1,3)]
   1  4    -5.40847406E-01   # Re[N(1,4)]
   2  1     2.56926427E-04   # Re[N(2,1)]
   2  2    -4.46002333E-04   # Re[N(2,2)]
   2  3    -7.07505083E-01   # Re[N(2,3)]
   2  4    -7.06708068E-01   # Re[N(2,4)]
   3  1     8.78812979E-01   # Re[N(3,1)]
   3  2     4.64437833E-01   # Re[N(3,2)]
   3  3    -7.73552036E-02   # Re[N(3,3)]
   3  4     7.74688336E-02   # Re[N(3,4)]
   4  1     2.97670355E-01   # Re[N(4,1)]
   4  2    -7.12919983E-01   # Re[N(4,2)]
   4  3    -4.48431808E-01   # Re[N(4,3)]
   4  4     4.49495685E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -6.12479611E-01   # Re[U(1,1)]
   1  2     7.90486386E-01   # Re[U(1,2)]
   2  1     7.90486386E-01   # Re[U(2,1)]
   2  2     6.12479611E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -6.13201551E-01   # Re[V(1,1)]
   1  2     7.89926489E-01   # Re[V(1,2)]
   2  1     7.89926489E-01   # Re[V(2,1)]
   2  2     6.13201551E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.69701627E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.55442038E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.67418675E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     7.71392188E-02    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.59350684E+01   # ~e^-_L
#    BR                NDA      ID1      ID2
     3.73551627E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.72412073E-01    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     8.40001096E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     2.59454833E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     3.46777790E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.69704859E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.55442026E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     7.67414125E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     7.71392993E-02    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
DECAY   1000013     1.59350896E+01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     3.73554504E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.72411778E-01    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     8.40002259E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     2.59454547E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     3.46777430E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     6.77699694E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.20086897E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     3.62948127E-04    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     6.43585268E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     8.40544956E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     5.55214462E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     9.63889442E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.48692515E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     4.50652364E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.60500745E-04    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.92634717E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     8.13740015E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.52982513E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.27783031E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.59111716E+01   # ~nu_e
#    BR                NDA      ID1      ID2
     1.84587974E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.08379173E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.60369137E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     3.46655657E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.59111884E+01   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.84587777E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.08378950E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.60369527E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     3.46655687E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.59158766E+01   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.84532425E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.08316100E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.60479483E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     3.46663936E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     2.99721968E-01   # ~d_R
#    BR                NDA      ID1      ID2
     1.74721910E-01    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     7.60310824E-01    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     6.49671645E-02    2     1000035         1   # BR(~d_R -> chi^0_4 d)
DECAY   1000001     6.79298205E+00   # ~d_L
#    BR                NDA      ID1      ID2
     1.55222145E-01    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     3.30531385E-02    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     1.53044896E-01    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     3.22540597E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     3.36139133E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
DECAY   2000003     2.99726473E-01   # ~s_R
#    BR                NDA      ID1      ID2
     1.74721578E-01    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     7.60299418E-01    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     6.49674631E-02    2     1000035         3   # BR(~s_R -> chi^0_4 s)
DECAY   1000003     6.79336092E+00   # ~s_L
#    BR                NDA      ID1      ID2
     1.55213404E-01    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.30512562E-02    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.53036266E-01    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     3.22552791E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     3.36146020E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
DECAY   1000005     2.75188751E+01   # ~b_1
#    BR                NDA      ID1      ID2
     3.16581129E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     6.41090792E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.76634200E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.44384195E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.69559246E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     2.20231577E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     3.42107598E-01   # ~b_2
#    BR                NDA      ID1      ID2
     1.69492897E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     9.15504676E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     6.66483962E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     5.63439705E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.71081128E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.02535608E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     4.11624509E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     1.20235355E+00   # ~u_R
#    BR                NDA      ID1      ID2
     1.74626134E-01    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     7.60349485E-01    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     6.50243092E-02    2     1000035         2   # BR(~u_R -> chi^0_4 u)
DECAY   1000002     6.79893525E+00   # ~u_L
#    BR                NDA      ID1      ID2
     9.33249118E-02    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.34339414E-01    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.13587209E-01    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.23158431E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     3.35589974E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
DECAY   2000004     1.20313367E+00   # ~c_R
#    BR                NDA      ID1      ID2
     1.74613274E-01    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.69499379E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     7.59843139E-01    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     6.50367622E-02    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     2.45389999E-04    2     1000024         3   # BR(~c_R -> chi^+_1 s)
DECAY   1000004     6.79946669E+00   # ~c_L
#    BR                NDA      ID1      ID2
     9.33354398E-02    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.34330275E-01    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.13593903E-01    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     3.23138188E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     3.35571722E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
DECAY   1000006     1.54691833E+01   # ~t_1
#    BR                NDA      ID1      ID2
     1.91949777E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     3.75724630E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.47008147E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     8.66820414E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.74717646E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.56209745E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     4.37615933E+01   # ~t_2
#    BR                NDA      ID1      ID2
     2.13495621E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.97225012E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.57441296E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.11414641E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.13610002E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.68307028E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.01266791E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.79570404E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.64036177E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     9.00546363E-07   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.41571201E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.26705838E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.13739497E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.13703938E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.04279526E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     4.06569999E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.75613615E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.66589193E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.54034264E-03    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     3.26021494E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.92291820E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.55774528E-02   # chi^0_2
#    BR                NDA      ID1      ID2
     8.55205806E-03    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     6.58063910E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     6.56136279E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     8.43766510E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     8.43756044E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     8.21829613E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     1.90414435E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.90410397E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.89198024E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.12836216E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     7.32744789E-02    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     7.32744789E-02    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     7.31554085E-02    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     7.31554085E-02    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     2.44248388E-02    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     2.44248388E-02    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     2.44246306E-02    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     2.44246306E-02    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     2.43477457E-02    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     2.43477457E-02    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     1.29406897E-02   # chi^0_3
#    BR                NDA      ID1      ID2
     3.90765247E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     3.90765247E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     2.17165821E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.27503482E-03    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     4.23115885E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.56221142E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.56221142E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.64285302E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     2.22586443E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.62331765E-04    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     6.95151925E-04    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000021     3.19588323E+02   # ~g
#    BR                NDA      ID1      ID2
     3.97607937E-02    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     3.97607937E-02    2    -2000002         2   # BR(~g -> ~u^*_R u)
     3.93264469E-02    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     3.93264469E-02    2    -1000002         2   # BR(~g -> ~u^*_L u)
     3.97613808E-02    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     3.97613808E-02    2    -2000004         4   # BR(~g -> ~c^*_R c)
     3.93258891E-02    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     3.93258891E-02    2    -1000004         4   # BR(~g -> ~c^*_L c)
     6.85461382E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     6.85461382E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.94412143E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     2.94412143E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     3.98438239E-02    2     2000001        -1   # BR(~g -> ~d_R d_bar)
     3.98438239E-02    2    -2000001         1   # BR(~g -> ~d^*_R d)
     3.93415615E-02    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     3.93415615E-02    2    -1000001         1   # BR(~g -> ~d^*_L d)
     3.98438266E-02    2     2000003        -3   # BR(~g -> ~s_R s_bar)
     3.98438266E-02    2    -2000003         3   # BR(~g -> ~s^*_R s)
     3.93415901E-02    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     3.93415901E-02    2    -1000003         3   # BR(~g -> ~s^*_L s)
     4.56031064E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     4.56031064E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     3.98312944E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     3.98312944E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
DECAY        25     2.50372600E-03   # h^0
#    BR                NDA      ID1      ID2
     2.58444304E-04    2          13       -13   # BR(h^0 -> mu^- mu^+)
     7.29300297E-02    2          15       -15   # BR(h^0 -> tau^- tau^+)
     3.20729602E-04    2           3        -3   # BR(h^0 -> s s_bar)
     8.13269266E-01    2           5        -5   # BR(h^0 -> b b_bar)
     5.35686233E-02    2           4        -4   # BR(h^0 -> c c_bar)
     5.81863893E-02    2          21        21   # BR(h^0 -> g g)
     1.27792981E-03    2          22        22   # BR(h^0 -> photon photon)
# writing decays into V V* as 3-body decays
#    BR                NDA      ID1      ID2       ID3
     9.37428723E-06    3          24        11        12   # BR(h^0 -> W^+ e^- nu_e)
     9.37428723E-06    3          24        13        14   # BR(h^0 -> W^+ mu^- nu_mu)
     9.37428723E-06    3          24        15        16   # BR(h^0 -> W^+ tau^- nu_tau)
     3.28100053E-05    3          24         1        -2   # BR(h^0 -> W^+ d u_bar)
     3.28100053E-05    3          24         3        -4   # BR(h^0 -> W^+ s c_bar)
     9.37428723E-06    3         -24       -11       -12   # BR(h^0 -> W^- e^+ nu_bar_e)
     9.37428723E-06    3         -24       -13       -14   # BR(h^0 -> W^- mu^+ nu_bar_mu)
     9.37428723E-06    3         -24       -15       -16   # BR(h^0 -> W^- tau^+ nu_bar_tau)
     3.28100053E-05    3         -24        -1         2   # BR(h^0 -> W^- d_bar u)
     3.28100053E-05    3         -24        -3         4   # BR(h^0 -> W^- s_bar c)
     0.00000000E+00    3          23        11       -11   # BR(h^0 -> Z e^- e^+)
     0.00000000E+00    3          23        13       -13   # BR(h^0 -> Z mu^- mu^+)
     0.00000000E+00    3          23        15       -15   # BR(h^0 -> Z tau^- tau^+)
     0.00000000E+00    3          23        12       -12   # BR(h^0 -> Z nu_e nu_bar_e)
     0.00000000E+00    3          23         1        -1   # BR(h^0 -> Z d d_bar)
     0.00000000E+00    3          23         3        -3   # BR(h^0 -> Z s s_bar)
     0.00000000E+00    3          23         5        -5   # BR(h^0 -> Z b b_bar)
     0.00000000E+00    3          23         2        -2   # BR(h^0 -> Z u u_bar)
     0.00000000E+00    3          23         4        -4   # BR(h^0 -> Z c c_bar)
DECAY        35     1.14160583E+02   # H^0
#    BR                NDA      ID1      ID2
     3.14977695E-04    2           5        -5   # BR(H^0 -> b b_bar)
     9.65053562E-01    2           6        -6   # BR(H^0 -> t t_bar)
     1.12260096E-02    2          23        23   # BR(H^0 -> Z Z)
     2.25332541E-02    2          24       -24   # BR(H^0 -> W^+ W^-)
     8.15527893E-04    2          21        21   # BR(H^0 -> g g)
DECAY        36     1.24670589E+02   # A^0
#    BR                NDA      ID1      ID2
     2.67989214E-04    2           5        -5   # BR(A^0 -> b b_bar)
     9.78933504E-01    2           6        -6   # BR(A^0 -> t t_bar)
     2.05174138E-02    2          25        23   # BR(A^0 -> h^0 Z)
     2.33383483E-04    2          21        21   # BR(A^0 -> g g)
DECAY        37     1.07829941E+02   # H^+
#    BR                NDA      ID1      ID2
     9.76079939E-01    2          -5         6   # BR(H^+ -> b_bar t)
     2.38684451E-02    2          25        24   # BR(H^+ -> h^0 W^+)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.24535895E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.07546410E+00    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.00000000E+00        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.07546410E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    9.24535895E-01    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.00000000E+00        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.24535895E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.07546410E+00    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.00000000E+00        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.98574259E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.42574051E-03        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.98574259E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.42574051E-03        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.36623136E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    9.41720920E-01        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.00000000E+00        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.42574051E-03        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.98574259E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.34634840E-04   # BR(b -> s gamma)
    2    1.62141308E-06   # BR(b -> s mu+ mu-)
    3    3.59378564E-05   # BR(b -> s nu nu)
    4    2.71533369E-15   # BR(Bd -> e+ e-)
    5    1.15990240E-10   # BR(Bd -> mu+ mu-)
    6    2.39431528E-08   # BR(Bd -> tau+ tau-)
    7    9.46408895E-14   # BR(Bs -> e+ e-)
    8    4.04265690E-09   # BR(Bs -> mu+ mu-)
    9    8.33635551E-07   # BR(Bs -> tau+ tau-)
   10    9.68067885E-05   # BR(B_u -> tau nu)
   11    9.99977063E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.65080472E-01   # |Delta(M_Bd)| [ps^-1] 
   13    2.01773521E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.25506658E-03   # epsilon_K
   17    2.28267435E-15   # Delta(M_K)
   18    2.52730144E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.40404226E-11   # BR(K^+ -> pi^+ nu nu)
   20    8.17878437E-17   # Delta(g-2)_electron/2
   21    3.49668494E-12   # Delta(g-2)_muon/2
   22    7.71195507E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -3.37938020E-03   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.03322607E-01   # C7
     0305 4322   00   2    -2.75418214E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.13320501E-01   # C8
     0305 6321   00   2    -3.48257096E-04   # C8'
 03051111 4133   00   0     1.59073070E+00   # C9 e+e-
 03051111 4133   00   2     1.59421551E+00   # C9 e+e-
 03051111 4233   00   2     2.99483178E-07   # C9' e+e-
 03051111 4137   00   0    -4.41342281E+00   # C10 e+e-
 03051111 4137   00   2    -4.47846873E+00   # C10 e+e-
 03051111 4237   00   2    -2.33955969E-06   # C10' e+e-
 03051313 4133   00   0     1.59073070E+00   # C9 mu+mu-
 03051313 4133   00   2     1.59421550E+00   # C9 mu+mu-
 03051313 4233   00   2     2.99483177E-07   # C9' mu+mu-
 03051313 4137   00   0    -4.41342281E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.47846873E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.33955969E-06   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.51954546E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     5.10022716E-07   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.51954546E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     5.10022716E-07   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.51954546E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     5.10022781E-07   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85814386E-07   # C7
     0305 4422   00   2     2.08265384E-06   # C7
     0305 4322   00   2    -2.38831800E-07   # C7'
     0305 6421   00   0     3.30475537E-07   # C8
     0305 6421   00   2    -3.03780232E-07   # C8
     0305 6321   00   2    -2.49507867E-07   # C8'
 03051111 4133   00   2     3.31731231E-06   # C9 e+e-
 03051111 4233   00   2     6.48969940E-09   # C9' e+e-
 03051111 4137   00   2    -1.80456054E-05   # C10 e+e-
 03051111 4237   00   2    -5.06976848E-08   # C10' e+e-
 03051313 4133   00   2     3.31731229E-06   # C9 mu+mu-
 03051313 4233   00   2     6.48969939E-09   # C9' mu+mu-
 03051313 4137   00   2    -1.80456054E-05   # C10 mu+mu-
 03051313 4237   00   2    -5.06976848E-08   # C10' mu+mu-
 03051212 4137   00   2     4.19286655E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     1.10520671E-08   # C11' nu_1 nu_1
 03051414 4137   00   2     4.19286655E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     1.10520671E-08   # C11' nu_2 nu_2
 03051616 4137   00   2     4.19286628E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     1.10520671E-08   # C11' nu_3 nu_3
