# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 13.10.2023,  16:21
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.51497939E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    1.49427391E+03  # scale for input parameters
    1    1.00000000E+03  # M_1
    2    1.00000000E+03  # M_2
    3    2.50000000E+03  # M_3
   11    2.82941176E+03  # A_t
   12    2.82941176E+03  # A_b
   13    2.82941176E+03  # A_tau
   23    1.00000000E+03  # mu
   25    3.40000000E+01  # tan(beta)
   26    4.50000000E+02  # m_A, pole mass
   31    2.00000000E+03  # M_L11
   32    2.00000000E+03  # M_L22
   33    2.00000000E+03  # M_L33
   34    2.00000000E+03  # M_E11
   35    2.00000000E+03  # M_E22
   36    2.00000000E+03  # M_E33
   41    1.50000000E+03  # M_Q11
   42    1.50000000E+03  # M_Q22
   43    1.50000000E+03  # M_Q33
   44    1.50000000E+03  # M_U11
   45    1.50000000E+03  # M_U22
   46    1.50000000E+03  # M_U33
   47    1.50000000E+03  # M_D11
   48    1.50000000E+03  # M_D22
   49    1.50000000E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  1.49427391E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  1.49427391E+03  # (SUSY scale)
  1  1     8.38799781E-06   # Y_u(Q)^DRbar
  2  2     4.26110289E-03   # Y_c(Q)^DRbar
  3  3     1.01333751E+00   # Y_t(Q)^DRbar
Block Yd Q=  1.49427391E+03  # (SUSY scale)
  1  1     5.73082930E-04   # Y_d(Q)^DRbar
  2  2     1.08885757E-02   # Y_s(Q)^DRbar
  3  3     5.68318689E-01   # Y_b(Q)^DRbar
Block Ye Q=  1.49427391E+03  # (SUSY scale)
  1  1     1.00007162E-04   # Y_e(Q)^DRbar
  2  2     2.06783093E-02   # Y_mu(Q)^DRbar
  3  3     3.47775145E-01   # Y_tau(Q)^DRbar
Block Au Q=  1.49427391E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.82941176E+03   # A_t(Q)^DRbar
Block Ad Q=  1.49427391E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     2.82941127E+03   # A_b(Q)^DRbar
Block Ae Q=  1.49427391E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     2.82941181E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  1.49427391E+03  # soft SUSY breaking masses at Q
   1    1.00000000E+03  # M_1
   2    1.00000000E+03  # M_2
   3    2.50000000E+03  # M_3
  21   -8.03618174E+05  # M^2_(H,d)
  22   -9.59793642E+05  # M^2_(H,u)
  31    2.00000000E+03  # M_(L,11)
  32    2.00000000E+03  # M_(L,22)
  33    2.00000000E+03  # M_(L,33)
  34    2.00000000E+03  # M_(E,11)
  35    2.00000000E+03  # M_(E,22)
  36    2.00000000E+03  # M_(E,33)
  41    1.50000000E+03  # M_(Q,11)
  42    1.50000000E+03  # M_(Q,22)
  43    1.50000000E+03  # M_(Q,33)
  44    1.50000000E+03  # M_(U,11)
  45    1.50000000E+03  # M_(U,22)
  46    1.50000000E+03  # M_(U,33)
  47    1.50000000E+03  # M_(D,11)
  48    1.50000000E+03  # M_(D,22)
  49    1.50000000E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22794890E+02  # h0
        35     4.49902193E+02  # H0
        36     4.50000000E+02  # A0
        37     4.57874484E+02  # H+
   1000001     1.55979023E+03  # ~d_L
   2000001     1.55164837E+03  # ~d_R
   1000002     1.55779973E+03  # ~u_L
   2000002     1.55200599E+03  # ~u_R
   1000003     1.55981347E+03  # ~s_L
   2000003     1.55162236E+03  # ~s_R
   1000004     1.55779891E+03  # ~c_L
   2000004     1.55200577E+03  # ~c_R
   1000005     1.50864196E+03  # ~b_1
   2000005     1.55070759E+03  # ~b_2
   1000006     1.36811828E+03  # ~t_1
   2000006     1.63206248E+03  # ~t_2
   1000011     2.00584975E+03  # ~e_L-
   2000011     2.00284102E+03  # ~e_R-
   1000012     2.00392824E+03  # ~nu_eL
   1000013     2.00609568E+03  # ~mu_L-
   2000013     2.00257844E+03  # ~mu_R-
   1000014     2.00392280E+03  # ~nu_muL
   1000015     1.98550479E+03  # ~tau_1-
   2000015     2.01412923E+03  # ~tau_2-
   1000016     2.00093879E+03  # ~nu_tauL
   1000021     2.43547759E+03  # ~g
   1000022     9.44009573E+02  # ~chi_10
   1000023     1.00112575E+03  # ~chi_20
   1000025     1.00512408E+03  # ~chi_30
   1000035     1.07594540E+03  # ~chi_40
   1000024     9.57345290E+02  # ~chi_1+
   1000037     1.07048317E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.28875380E-02   # alpha
Block Hmix Q=  1.49427391E+03  # Higgs mixing parameters
   1    1.00000000E+03  # mu
   2    3.40000000E+01  # tan[beta](Q)
   3    2.44031639E+02  # v(Q)
   4    2.02500000E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -6.70067901E-01   # Re[R_st(1,1)]
   1  2     7.42299811E-01   # Re[R_st(1,2)]
   2  1    -7.42299811E-01   # Re[R_st(2,1)]
   2  2    -6.70067901E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     8.88468446E-01   # Re[R_sb(1,1)]
   1  2     4.58937708E-01   # Re[R_sb(1,2)]
   2  1    -4.58937708E-01   # Re[R_sb(2,1)]
   2  2     8.88468446E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     6.32814607E-01   # Re[R_sta(1,1)]
   1  2     7.74303347E-01   # Re[R_sta(1,2)]
   2  1    -7.74303347E-01   # Re[R_sta(2,1)]
   2  2     6.32814607E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     4.50557889E-01   # Re[N(1,1)]
   1  2    -5.18097203E-01   # Re[N(1,2)]
   1  3     5.28945529E-01   # Re[N(1,3)]
   1  4    -4.98788035E-01   # Re[N(1,4)]
   2  1     8.47688216E-01   # Re[N(2,1)]
   2  2     5.03712416E-01   # Re[N(2,2)]
   2  3    -1.18183727E-01   # Re[N(2,3)]
   2  4     1.17179767E-01   # Re[N(2,4)]
   3  1     1.52514587E-02   # Re[N(3,1)]
   3  2    -2.68281249E-02   # Re[N(3,2)]
   3  3    -7.06241288E-01   # Re[N(3,3)]
   3  4    -7.07298302E-01   # Re[N(3,4)]
   4  1    -2.79624159E-01   # Re[N(4,1)]
   4  2     6.90745497E-01   # Re[N(4,2)]
   4  3     4.55491468E-01   # Re[N(4,3)]
   4  4    -4.87040564E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -6.33266519E-01   # Re[U(1,1)]
   1  2     7.73933793E-01   # Re[U(1,2)]
   2  1     7.73933793E-01   # Re[U(2,1)]
   2  2     6.33266519E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -6.73834624E-01   # Re[V(1,1)]
   1  2     7.38882196E-01   # Re[V(1,2)]
   2  1     7.38882196E-01   # Re[V(2,1)]
   2  2     6.73834624E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.70485920E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     2.16666241E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.13298418E-01    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.29688925E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     6.98056128E-02    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.59134812E+01   # ~e^-_L
#    BR                NDA      ID1      ID2
     2.58747569E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.86236520E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.08590284E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     8.24909714E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     2.65342526E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     3.39946636E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.72224868E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     2.16449664E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     7.10937273E-01    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.05993574E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     6.98828106E-02    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     1.06125531E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     6.09061377E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.59277583E+01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     2.60397647E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.86069755E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     4.08310467E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     8.25567380E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     2.65175537E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     3.39749895E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.44337362E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.22269877E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.93394974E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     8.53219428E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.12108872E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.63701993E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.31901238E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.51648634E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     3.81605847E-04    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.91427528E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     9.60063649E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.79993895E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.84549654E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.93735364E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.59364085E+01   # ~nu_e
#    BR                NDA      ID1      ID2
     1.92928466E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     8.06217081E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.79245921E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     1.97879708E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.99369273E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     3.08637089E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.59458762E+01   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.92812796E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     8.05733301E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.79018335E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     1.97760813E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.99569741E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     3.08671898E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.85774796E+01   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.64972105E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     6.89196430E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.24192300E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.69084532E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.47930563E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     3.16999376E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     3.00191947E-01   # ~d_R
#    BR                NDA      ID1      ID2
     2.32408327E-01    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     7.06529967E-01    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     2.31793341E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     6.08184350E-02    2     1000035         1   # BR(~d_R -> chi^0_4 d)
DECAY   1000001     6.75969647E+00   # ~d_L
#    BR                NDA      ID1      ID2
     1.46019221E-01    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.37051545E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     3.03388136E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     1.53034631E-01    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     3.16466924E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     3.40470681E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
DECAY   2000003     3.02652791E-01   # ~s_R
#    BR                NDA      ID1      ID2
     2.31834160E-01    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     7.00801414E-01    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     2.25626265E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     6.09984681E-02    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     2.77807100E-03    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
     1.33162445E-03    2    -1000037         4   # BR(~s_R -> chi^-_2 c)
DECAY   1000003     6.76153495E+00   # ~s_L
#    BR                NDA      ID1      ID2
     1.46046018E-01    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.36981479E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     3.95978029E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.53036166E-01    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     3.16404925E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     3.40418765E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
DECAY   1000005     1.71094487E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.55992407E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.74789175E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     8.41015881E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     3.40865283E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.37940009E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.21055495E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     1.49345055E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     1.16745481E+01   # ~b_2
#    BR                NDA      ID1      ID2
     2.52856439E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.23667424E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.48713173E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.26952366E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     1.77827389E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     3.37154506E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.61700180E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     1.20178854E+00   # ~u_R
#    BR                NDA      ID1      ID2
     2.32386841E-01    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     7.06556426E-01    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     2.26115127E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     6.08306171E-02    2     1000035         2   # BR(~u_R -> chi^0_4 u)
DECAY   1000002     6.80771245E+00   # ~u_L
#    BR                NDA      ID1      ID2
     7.73206144E-02    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.48737381E-01    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.99505275E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.13161024E-01    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.54233929E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     3.06347547E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
DECAY   2000004     1.20215745E+00   # ~c_R
#    BR                NDA      ID1      ID2
     2.32349040E-01    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     7.06342996E-01    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.04141101E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     6.08479148E-02    2     1000035         4   # BR(~c_R -> chi^0_4 c)
DECAY   1000004     6.80913152E+00   # ~c_L
#    BR                NDA      ID1      ID2
     7.73095190E-02    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.48704820E-01    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     2.13335363E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.13145577E-01    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     3.54280435E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     3.06346314E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
DECAY   1000006     1.14032452E+01   # ~t_1
#    BR                NDA      ID1      ID2
     6.19891700E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     8.35858086E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.08949900E-02    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     3.05610093E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     3.04232377E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.95424984E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     9.53650138E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.73638440E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     3.41398935E+01   # ~t_2
#    BR                NDA      ID1      ID2
     1.52126933E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     4.14040543E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.42545832E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.46313638E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.27442668E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.45951425E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.06311722E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     2.32400690E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     4.19077573E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     8.74604270E-07   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.41620487E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.26783189E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.13740314E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.13704162E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.04151848E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     3.81452536E-01   # chi^+_2
#    BR                NDA      ID1      ID2
     5.35901015E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     4.45891502E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
#    BR                NDA      ID1      ID2       ID3
     1.48681560E-03    3     1000023        -1         2   # BR(chi^+_2 -> chi^0_2 d_bar u)
     1.48531927E-03    3     1000023        -3         4   # BR(chi^+_2 -> chi^0_2 s_bar c)
     4.94766979E-04    3     1000023       -11        12   # BR(chi^+_2 -> chi^0_2 e^+ nu_e)
     4.94762673E-04    3     1000023       -13        14   # BR(chi^+_2 -> chi^0_2 mu^+ nu_mu)
     4.92811376E-04    3     1000023       -15        16   # BR(chi^+_2 -> chi^0_2 tau^+ nu_tau)
     4.57832487E-03    3     1000025        -1         2   # BR(chi^+_2 -> chi^0_3 d_bar u)
     4.57303377E-03    3     1000025        -3         4   # BR(chi^+_2 -> chi^0_3 s_bar c)
     1.52603982E-03    3     1000025       -11        12   # BR(chi^+_2 -> chi^0_3 e^+ nu_e)
     1.52603340E-03    3     1000025       -13        14   # BR(chi^+_2 -> chi^0_3 mu^+ nu_mu)
     1.52350275E-03    3     1000025       -15        16   # BR(chi^+_2 -> chi^0_3 tau^+ nu_tau)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.18376723E-04   # chi^0_2
#    BR                NDA      ID1      ID2
     1.18217164E-04    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.67110547E-01    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     1.67110547E-01    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     1.66493167E-01    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     1.66493167E-01    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     5.55930011E-02    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     5.55930011E-02    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     5.55922975E-02    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     5.55922975E-02    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     5.50361077E-02    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     5.50361077E-02    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     4.20917799E-03   # chi^0_3
#    BR                NDA      ID1      ID2
     1.40098885E-02    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     6.97852934E-02    3     1000022         2        -2   # BR(chi^0_3 -> chi^0_1 u u_bar)
     6.94642894E-02    3     1000022         4        -4   # BR(chi^0_3 -> chi^0_1 c c_bar)
     8.94506120E-02    3     1000022         1        -1   # BR(chi^0_3 -> chi^0_1 d d_bar)
     8.94489183E-02    3     1000022         3        -3   # BR(chi^0_3 -> chi^0_1 s s_bar)
     8.64315411E-02    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     2.01890503E-02    3     1000022        11       -11   # BR(chi^0_3 -> chi^0_1 e^- e^+)
     2.01884659E-02    3     1000022        13       -13   # BR(chi^0_3 -> chi^0_1 mu^- mu^+)
     2.00254152E-02    3     1000022        15       -15   # BR(chi^0_3 -> chi^0_1 tau^- tau^+)
     1.19656871E-01    3     1000022        12       -12   # BR(chi^0_3 -> chi^0_1 nu_e nu_bar_e)
     6.69902803E-02    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     6.69902803E-02    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     6.68014017E-02    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     6.68014017E-02    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     2.23309275E-02    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     2.23309275E-02    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     2.23306547E-02    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     2.23306547E-02    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     2.22205017E-02    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     2.22205017E-02    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     4.98936467E-01   # chi^0_4
#    BR                NDA      ID1      ID2
     4.92210096E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     4.92210096E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.89007662E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.37179538E-03    3     1000025         2        -2   # BR(chi^0_4 -> chi^0_3 u u_bar)
     1.36754529E-03    3     1000025         4        -4   # BR(chi^0_4 -> chi^0_3 c c_bar)
     1.75964146E-03    3     1000025         1        -1   # BR(chi^0_4 -> chi^0_3 d d_bar)
     1.75962253E-03    3     1000025         3        -3   # BR(chi^0_4 -> chi^0_3 s s_bar)
     1.72198310E-03    3     1000025         5        -5   # BR(chi^0_4 -> chi^0_3 b b_bar)
     3.97035617E-04    3     1000025        11       -11   # BR(chi^0_4 -> chi^0_3 e^- e^+)
     3.97028783E-04    3     1000025        13       -13   # BR(chi^0_4 -> chi^0_3 mu^- mu^+)
     3.94921712E-04    3     1000025        15       -15   # BR(chi^0_4 -> chi^0_3 tau^- tau^+)
     2.35231281E-03    3     1000025        12       -12   # BR(chi^0_4 -> chi^0_3 nu_e nu_bar_e)
DECAY   1000021     3.19130134E+02   # ~g
#    BR                NDA      ID1      ID2
     3.98404734E-02    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     3.98404734E-02    2    -2000002         2   # BR(~g -> ~u^*_R u)
     3.94339896E-02    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     3.94339896E-02    2    -1000002         2   # BR(~g -> ~u^*_L u)
     3.98404639E-02    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     3.98404639E-02    2    -2000004         4   # BR(~g -> ~c^*_R c)
     3.94340220E-02    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     3.94340220E-02    2    -1000004         4   # BR(~g -> ~c^*_L c)
     1.18034432E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.18034432E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     6.33234434E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     6.33234434E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.51643818E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     2.51643818E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     3.98655822E-02    2     2000001        -1   # BR(~g -> ~d_R d_bar)
     3.98655822E-02    2    -2000001         1   # BR(~g -> ~d^*_R d)
     3.92944708E-02    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     3.92944708E-02    2    -1000001         1   # BR(~g -> ~d^*_L d)
     3.98674084E-02    2     2000003        -3   # BR(~g -> ~s_R s_bar)
     3.98674084E-02    2    -2000003         3   # BR(~g -> ~s^*_R s)
     3.92928422E-02    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     3.92928422E-02    2    -1000003         3   # BR(~g -> ~s^*_L s)
     4.27038825E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     4.27038825E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     4.01193680E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     4.01193680E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
DECAY        25     5.07258876E-03   # h^0
#    BR                NDA      ID1      ID2
     2.25703896E-04    2          13       -13   # BR(h^0 -> mu^- mu^+)
     6.30374526E-02    2          15       -15   # BR(h^0 -> tau^- tau^+)
     2.72372525E-04    2           3        -3   # BR(h^0 -> s s_bar)
     6.91976812E-01    2           5        -5   # BR(h^0 -> b b_bar)
     3.61451373E-02    2           4        -4   # BR(h^0 -> c c_bar)
     7.10101449E-02    2          21        21   # BR(h^0 -> g g)
     2.04579508E-03    2          22        22   # BR(h^0 -> photon photon)
# writing decays into V V* as 3-body decays
#    BR                NDA      ID1      ID2       ID3
     6.13927945E-03    3          24        11        12   # BR(h^0 -> W^+ e^- nu_e)
     6.13927945E-03    3          24        13        14   # BR(h^0 -> W^+ mu^- nu_mu)
     6.13927945E-03    3          24        15        16   # BR(h^0 -> W^+ tau^- nu_tau)
     2.14874781E-02    3          24         1        -2   # BR(h^0 -> W^+ d u_bar)
     2.14874781E-02    3          24         3        -4   # BR(h^0 -> W^+ s c_bar)
     6.13927945E-03    3         -24       -11       -12   # BR(h^0 -> W^- e^+ nu_bar_e)
     6.13927945E-03    3         -24       -13       -14   # BR(h^0 -> W^- mu^+ nu_bar_mu)
     6.13927945E-03    3         -24       -15       -16   # BR(h^0 -> W^- tau^+ nu_bar_tau)
     2.14874781E-02    3         -24        -1         2   # BR(h^0 -> W^- d_bar u)
     2.14874781E-02    3         -24        -3         4   # BR(h^0 -> W^- s_bar c)
     4.20003111E-04    3          23        11       -11   # BR(h^0 -> Z e^- e^+)
     4.20003111E-04    3          23        13       -13   # BR(h^0 -> Z mu^- mu^+)
     4.22503129E-04    3          23        15       -15   # BR(h^0 -> Z tau^- tau^+)
     2.50001852E-03    3          23        12       -12   # BR(h^0 -> Z nu_e nu_bar_e)
     1.95001444E-03    3          23         1        -1   # BR(h^0 -> Z d d_bar)
     1.95001444E-03    3          23         3        -3   # BR(h^0 -> Z s s_bar)
     1.88751398E-03    3          23         5        -5   # BR(h^0 -> Z b b_bar)
     1.45001074E-03    3          23         2        -2   # BR(h^0 -> Z u u_bar)
     1.50001111E-03    3          23         4        -4   # BR(h^0 -> Z c c_bar)
DECAY        35     1.08905242E+01   # H^0
#    BR                NDA      ID1      ID2
     3.51042395E-04    2          13       -13   # BR(H^0 -> mu^- mu^+)
     9.92855591E-02    2          15       -15   # BR(H^0 -> tau^- tau^+)
     3.29916298E-04    2           3        -3   # BR(H^0 -> s s_bar)
     8.98673271E-01    2           5        -5   # BR(H^0 -> b b_bar)
     1.07574469E-03    2           6        -6   # BR(H^0 -> t t_bar)
     1.66157970E-04    2          21        21   # BR(H^0 -> g g)
DECAY        36     1.09031569E+01   # A^0
#    BR                NDA      ID1      ID2
     3.50788122E-04    2          13       -13   # BR(A^0 -> mu^- mu^+)
     9.92198140E-02    2          15       -15   # BR(A^0 -> tau^- tau^+)
     3.29677903E-04    2           3        -3   # BR(A^0 -> s s_bar)
     8.98111848E-01    2           5        -5   # BR(A^0 -> b b_bar)
     1.92266716E-03    2           6        -6   # BR(A^0 -> t t_bar)
DECAY        37     7.59460365E+00   # H^+
#    BR                NDA      ID1      ID2
     5.12419864E-04    2         -13        12   # BR(H^+ -> mu^+ nu_e)
     1.44937226E-01    2         -15        12   # BR(H^+ -> tau^+ nu_e)
     4.26294913E-04    2          -3         4   # BR(H^+ -> s_bar c)
     8.54085784E-01    2          -5         6   # BR(H^+ -> b_bar t)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.25094871E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.15574905E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.15600000E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99782916E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.08213556E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    8.65051903E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.25094871E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.15574905E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.15600000E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99987860E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.21399475E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99987860E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.21399475E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.23501000E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    5.99121780E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.60996268E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.21399475E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99987860E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    4.44238816E-04   # BR(b -> s gamma)
    2    1.59088841E-06   # BR(b -> s mu+ mu-)
    3    3.51980213E-05   # BR(b -> s nu nu)
    4    1.92190645E-15   # BR(Bd -> e+ e-)
    5    8.20788673E-11   # BR(Bd -> mu+ mu-)
    6    1.58514311E-08   # BR(Bd -> tau+ tau-)
    7    6.33426285E-14   # BR(Bs -> e+ e-)
    8    2.70516444E-09   # BR(Bs -> mu+ mu-)
    9    5.24646432E-07   # BR(Bs -> tau+ tau-)
   10    5.53052662E-05   # BR(B_u -> tau nu)
   11    5.71282226E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.43491006E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.92728599E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16829654E-03   # epsilon_K
   17    2.28180815E-15   # Delta(M_K)
   18    2.47712277E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28177512E-11   # BR(K^+ -> pi^+ nu nu)
   20    4.61448283E-15   # Delta(g-2)_electron/2
   21    1.97281030E-10   # Delta(g-2)_muon/2
   22    5.56675719E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    8.99076250E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -3.10887132E-01   # C7
     0305 4322   00   2    -2.39387850E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -2.08008377E-01   # C8
     0305 6321   00   2    -2.19717187E-03   # C8'
 03051111 4133   00   0     1.59200967E+00   # C9 e+e-
 03051111 4133   00   2     1.59272702E+00   # C9 e+e-
 03051111 4233   00   2    -2.56297034E-04   # C9' e+e-
 03051111 4137   00   0    -4.41470177E+00   # C10 e+e-
 03051111 4137   00   2    -4.40758898E+00   # C10 e+e-
 03051111 4237   00   2     2.00023303E-03   # C10' e+e-
 03051313 4133   00   0     1.59200967E+00   # C9 mu+mu-
 03051313 4133   00   2     1.59272539E+00   # C9 mu+mu-
 03051313 4233   00   2    -2.56395839E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.41470177E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.40759061E+00   # C10 mu+mu-
 03051313 4237   00   2     2.00033060E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50380561E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -4.35923069E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50380561E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -4.35901593E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50380561E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -4.29849414E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85707104E-07   # C7
     0305 4422   00   2     3.24208780E-05   # C7
     0305 4322   00   2    -1.09381731E-05   # C7'
     0305 6421   00   0     3.30383643E-07   # C8
     0305 6421   00   2     1.25527050E-06   # C8
     0305 6321   00   2    -5.79613124E-06   # C8'
 03051111 4133   00   2    -3.33235332E-06   # C9 e+e-
 03051111 4233   00   2     2.57103036E-06   # C9' e+e-
 03051111 4137   00   2     2.88353135E-05   # C10 e+e-
 03051111 4237   00   2    -2.00089492E-05   # C10' e+e-
 03051313 4133   00   2    -3.33232017E-06   # C9 mu+mu-
 03051313 4233   00   2     2.57102465E-06   # C9' mu+mu-
 03051313 4137   00   2     2.88353566E-05   # C10 mu+mu-
 03051313 4237   00   2    -2.00089675E-05   # C10' mu+mu-
 03051212 4137   00   2    -6.17036409E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     4.36068153E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -6.17036415E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     4.36068153E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -6.17040126E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     4.36068154E-06   # C11' nu_3 nu_3
