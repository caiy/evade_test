# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 13.10.2023,  16:24
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.12321225E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    1.49593652E+03  # scale for input parameters
    1    1.00000000E+03  # M_1
    2    1.00000000E+03  # M_2
    3    2.50000000E+03  # M_3
   11    2.82500000E+03  # A_t
   12    2.82500000E+03  # A_b
   13    2.82500000E+03  # A_tau
   23    1.00000000E+03  # mu
   25    4.00000000E+01  # tan(beta)
   26    8.50000000E+02  # m_A, pole mass
   31    2.00000000E+03  # M_L11
   32    2.00000000E+03  # M_L22
   33    2.00000000E+03  # M_L33
   34    2.00000000E+03  # M_E11
   35    2.00000000E+03  # M_E22
   36    2.00000000E+03  # M_E33
   41    1.50000000E+03  # M_Q11
   42    1.50000000E+03  # M_Q22
   43    1.50000000E+03  # M_Q33
   44    1.50000000E+03  # M_U11
   45    1.50000000E+03  # M_U22
   46    1.50000000E+03  # M_U33
   47    1.50000000E+03  # M_D11
   48    1.50000000E+03  # M_D22
   49    1.50000000E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  1.49593652E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  1.49593652E+03  # (SUSY scale)
  1  1     8.38699184E-06   # Y_u(Q)^DRbar
  2  2     4.26059186E-03   # Y_c(Q)^DRbar
  3  3     1.01321598E+00   # Y_t(Q)^DRbar
Block Yd Q=  1.49593652E+03  # (SUSY scale)
  1  1     6.74134354E-04   # Y_d(Q)^DRbar
  2  2     1.28085527E-02   # Y_s(Q)^DRbar
  3  3     6.68530036E-01   # Y_b(Q)^DRbar
Block Ye Q=  1.49593652E+03  # (SUSY scale)
  1  1     1.17641374E-04   # Y_e(Q)^DRbar
  2  2     2.43245051E-02   # Y_mu(Q)^DRbar
  3  3     4.09098161E-01   # Y_tau(Q)^DRbar
Block Au Q=  1.49593652E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.82500000E+03   # A_t(Q)^DRbar
Block Ad Q=  1.49593652E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     2.82499946E+03   # A_b(Q)^DRbar
Block Ae Q=  1.49593652E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     2.82500004E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  1.49593652E+03  # soft SUSY breaking masses at Q
   1    1.00000000E+03  # M_1
   2    1.00000000E+03  # M_2
   3    2.50000000E+03  # M_3
  21   -2.77685001E+05  # M^2_(H,d)
  22   -9.59997335E+05  # M^2_(H,u)
  31    2.00000000E+03  # M_(L,11)
  32    2.00000000E+03  # M_(L,22)
  33    2.00000000E+03  # M_(L,33)
  34    2.00000000E+03  # M_(E,11)
  35    2.00000000E+03  # M_(E,22)
  36    2.00000000E+03  # M_(E,33)
  41    1.50000000E+03  # M_(Q,11)
  42    1.50000000E+03  # M_(Q,22)
  43    1.50000000E+03  # M_(Q,33)
  44    1.50000000E+03  # M_(U,11)
  45    1.50000000E+03  # M_(U,22)
  46    1.50000000E+03  # M_(U,33)
  47    1.50000000E+03  # M_(D,11)
  48    1.50000000E+03  # M_(D,22)
  49    1.50000000E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22855652E+02  # h0
        35     8.49895800E+02  # H0
        36     8.50000000E+02  # A0
        37     8.54362698E+02  # H+
   1000001     1.55983642E+03  # ~d_L
   2000001     1.55174112E+03  # ~d_R
   1000002     1.55784096E+03  # ~u_L
   2000002     1.55181981E+03  # ~u_R
   1000003     1.55986683E+03  # ~s_L
   2000003     1.55170670E+03  # ~s_R
   1000004     1.55783974E+03  # ~c_L
   2000004     1.55181962E+03  # ~c_R
   1000005     1.50717367E+03  # ~b_1
   2000005     1.55329243E+03  # ~b_2
   1000006     1.36961312E+03  # ~t_1
   2000006     1.63391109E+03  # ~t_2
   1000011     2.00574866E+03  # ~e_L-
   2000011     2.00303873E+03  # ~e_R-
   1000012     2.00382453E+03  # ~nu_eL
   1000013     2.00610916E+03  # ~mu_L-
   2000013     2.00265247E+03  # ~mu_R-
   1000014     2.00381609E+03  # ~nu_muL
   1000015     1.98221716E+03  # ~tau_1-
   2000015     2.01618359E+03  # ~tau_2-
   1000016     2.00040033E+03  # ~nu_tauL
   1000021     2.43547946E+03  # ~g
   1000022     9.45043552E+02  # ~chi_10
   1000023     1.00173221E+03  # ~chi_20
   1000025     1.00591283E+03  # ~chi_30
   1000035     1.07680053E+03  # ~chi_40
   1000024     9.58477873E+02  # ~chi_1+
   1000037     1.07146683E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.52949498E-02   # alpha
Block Hmix Q=  1.49593652E+03  # Higgs mixing parameters
   1    1.00000000E+03  # mu
   2    4.00000000E+01  # tan[beta](Q)
   3    2.44031736E+02  # v(Q)
   4    7.22500000E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -6.71053031E-01   # Re[R_st(1,1)]
   1  2     7.41409354E-01   # Re[R_st(1,2)]
   2  1    -7.41409354E-01   # Re[R_st(2,1)]
   2  2    -6.71053031E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     8.67048398E-01   # Re[R_sb(1,1)]
   1  2     4.98223921E-01   # Re[R_sb(1,2)]
   2  1    -4.98223921E-01   # Re[R_sb(2,1)]
   2  2     8.67048398E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     6.44116439E-01   # Re[R_sta(1,1)]
   1  2     7.64927456E-01   # Re[R_sta(1,2)]
   2  1    -7.64927456E-01   # Re[R_sta(2,1)]
   2  2     6.44116439E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     4.54008554E-01   # Re[N(1,1)]
   1  2    -5.15094617E-01   # Re[N(1,2)]
   1  3     5.28974009E-01   # Re[N(1,3)]
   1  4    -4.98738676E-01   # Re[N(1,4)]
   2  1     8.46804600E-01   # Re[N(2,1)]
   2  2     5.03209740E-01   # Re[N(2,2)]
   2  3    -1.22367943E-01   # Re[N(2,3)]
   2  4     1.21359030E-01   # Re[N(2,4)]
   3  1     1.53151892E-02   # Re[N(3,1)]
   3  2    -2.69337881E-02   # Re[N(3,2)]
   3  3    -7.06239067E-01   # Re[N(3,3)]
   3  4    -7.07295126E-01   # Re[N(3,4)]
   4  1    -2.76701370E-01   # Re[N(4,1)]
   4  2     6.93348443E-01   # Re[N(4,2)]
   4  3     4.54355548E-01   # Re[N(4,3)]
   4  4    -4.86071315E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -6.31393894E-01   # Re[U(1,1)]
   1  2     7.75462282E-01   # Re[U(1,2)]
   2  1     7.75462282E-01   # Re[U(2,1)]
   2  2     6.31393894E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -6.72202943E-01   # Re[V(1,1)]
   1  2     7.40366938E-01   # Re[V(1,2)]
   2  1     7.40366938E-01   # Re[V(2,1)]
   2  2     6.72202943E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.70306414E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     2.19838058E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.11625407E-01    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.31501560E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     6.83049787E-02    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.58850839E+01   # ~e^-_L
#    BR                NDA      ID1      ID2
     2.49893815E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.85900513E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.09494441E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     8.37958580E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     2.63849105E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     3.41355648E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.72695004E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     2.19524915E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     7.08373738E-01    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.37827130E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     6.84159391E-02    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     1.47116254E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     8.35973934E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.59052117E+01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     2.52185454E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.85670508E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     5.24381412E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     8.38851158E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     2.63618633E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     3.41082816E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.61821727E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.32699156E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.61729038E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.05786270E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     8.63033683E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.76777785E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     2.21447177E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.64518957E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     3.04907852E-03    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.68911305E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.21512734E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.91518039E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.99707434E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.95037480E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.59078042E+01   # ~nu_e
#    BR                NDA      ID1      ID2
     1.92423133E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     8.05946747E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.82518518E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     1.98444477E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.98005405E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     3.09938520E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.59208723E+01   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.92263452E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     8.05277300E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.82200761E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     1.98279399E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.98286266E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     3.09983405E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.95696615E+01   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.55843855E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     6.52526796E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.09694411E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.60588322E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.62436150E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     3.20169372E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     2.99913375E-01   # ~d_R
#    BR                NDA      ID1      ID2
     2.35641563E-01    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     7.04655902E-01    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     2.35605339E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     5.94510645E-02    2     1000035         1   # BR(~d_R -> chi^0_4 d)
DECAY   1000001     6.73473842E+00   # ~d_L
#    BR                NDA      ID1      ID2
     1.45038912E-01    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.37118985E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     3.06377609E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     1.54042451E-01    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     3.14892869E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     3.42007492E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
DECAY   2000003     3.03312687E-01   # ~s_R
#    BR                NDA      ID1      ID2
     2.34815337E-01    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     6.96788748E-01    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     3.02497175E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     5.97083286E-02    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     3.84048499E-03    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
     1.82213012E-03    2    -1000037         4   # BR(~s_R -> chi^-_2 c)
DECAY   1000003     6.73718194E+00   # ~s_L
#    BR                NDA      ID1      ID2
     1.45078203E-01    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.37029174E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     4.34672100E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.54045101E-01    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     3.14807693E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     3.41931414E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
DECAY   1000005     1.75834656E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.83814439E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.80856531E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.12781730E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     3.32251310E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.39829836E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.85805345E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     1.26457866E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     1.48773647E+01   # ~b_2
#    BR                NDA      ID1      ID2
     3.31450989E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.90811467E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.61697367E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.28460331E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     1.77378693E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     3.27022307E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.53215057E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     1.19984610E+00   # ~u_R
#    BR                NDA      ID1      ID2
     2.35639080E-01    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     7.04680223E-01    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     2.27753588E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     5.94529435E-02    2     1000035         2   # BR(~u_R -> chi^0_4 u)
DECAY   1000002     6.78291788E+00   # ~u_L
#    BR                NDA      ID1      ID2
     7.61391007E-02    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.48741303E-01    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     2.01372898E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.14365926E-01    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.52825339E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     3.07726958E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
DECAY   2000004     1.20021397E+00   # ~c_R
#    BR                NDA      ID1      ID2
     2.35600182E-01    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     7.04468025E-01    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.05668777E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     5.94704413E-02    2     1000035         4   # BR(~c_R -> chi^0_4 c)
DECAY   1000004     6.78480836E+00   # ~c_L
#    BR                NDA      ID1      ID2
     7.61228650E-02    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.48698141E-01    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     2.15203513E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.14342050E-01    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     3.52895087E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     3.07726653E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
DECAY   1000006     1.17692651E+01   # ~t_1
#    BR                NDA      ID1      ID2
     5.98632447E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     8.11142800E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.06998408E-02    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     3.02892635E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.95446165E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.89664077E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.13409210E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.73273812E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.17026454E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     3.52132497E+01   # ~t_2
#    BR                NDA      ID1      ID2
     1.47527729E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     4.04528572E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.38545493E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.38325384E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.34261372E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     3.17973757E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.32535230E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     2.26457020E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     3.86881766E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     9.01100565E-07   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.41452647E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.26835397E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.13684864E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.13649864E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.04377228E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     3.76947246E-01   # chi^+_2
#    BR                NDA      ID1      ID2
     5.35479987E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     4.45702144E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
#    BR                NDA      ID1      ID2       ID3
     1.55148742E-03    3     1000023        -1         2   # BR(chi^+_2 -> chi^0_2 d_bar u)
     1.54996443E-03    3     1000023        -3         4   # BR(chi^+_2 -> chi^0_2 s_bar c)
     5.16291469E-04    3     1000023       -11        12   # BR(chi^+_2 -> chi^0_2 e^+ nu_e)
     5.16287950E-04    3     1000023       -13        14   # BR(chi^+_2 -> chi^0_2 mu^+ nu_mu)
     5.14583999E-04    3     1000023       -15        16   # BR(chi^+_2 -> chi^0_2 tau^+ nu_tau)
     4.72337733E-03    3     1000025        -1         2   # BR(chi^+_2 -> chi^0_3 d_bar u)
     4.71797808E-03    3     1000025        -3         4   # BR(chi^+_2 -> chi^0_3 s_bar c)
     1.57438818E-03    3     1000025       -11        12   # BR(chi^+_2 -> chi^0_3 e^+ nu_e)
     1.57438156E-03    3     1000025       -13        14   # BR(chi^+_2 -> chi^0_3 mu^+ nu_mu)
     1.57177853E-03    3     1000025       -15        16   # BR(chi^+_2 -> chi^0_3 tau^+ nu_tau)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.00160402E-04   # chi^0_2
#    BR                NDA      ID1      ID2
     1.21694233E-04    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.67097580E-01    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     1.67097580E-01    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     1.66463217E-01    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     1.66463217E-01    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     5.55889773E-02    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     5.55889773E-02    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     5.55885160E-02    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     5.55885160E-02    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     5.50912916E-02    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     5.50912916E-02    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     4.08265712E-03   # chi^0_3
#    BR                NDA      ID1      ID2
     1.41534172E-02    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     7.01327633E-02    3     1000022         2        -2   # BR(chi^0_3 -> chi^0_1 u u_bar)
     6.98068274E-02    3     1000022         4        -4   # BR(chi^0_3 -> chi^0_1 c c_bar)
     8.98959024E-02    3     1000022         1        -1   # BR(chi^0_3 -> chi^0_1 d d_bar)
     8.98942635E-02    3     1000022         3        -3   # BR(chi^0_3 -> chi^0_1 s s_bar)
     8.70527730E-02    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     2.02895643E-02    3     1000022        11       -11   # BR(chi^0_3 -> chi^0_1 e^- e^+)
     2.02890123E-02    3     1000022        13       -13   # BR(chi^0_3 -> chi^0_1 mu^- mu^+)
     2.01353747E-02    3     1000022        15       -15   # BR(chi^0_3 -> chi^0_1 tau^- tau^+)
     1.20252718E-01    3     1000022        12       -12   # BR(chi^0_3 -> chi^0_1 nu_e nu_bar_e)
     6.64474933E-02    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     6.64474933E-02    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     6.62567445E-02    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     6.62567445E-02    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     2.21499946E-02    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     2.21499946E-02    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     2.21497367E-02    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     2.21497367E-02    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     2.20434710E-02    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     2.20434710E-02    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     4.95659496E-01   # chi^0_4
#    BR                NDA      ID1      ID2
     4.92084933E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     4.92084933E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.02601664E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.38455490E-03    3     1000025         2        -2   # BR(chi^0_4 -> chi^0_3 u u_bar)
     1.38027720E-03    3     1000025         4        -4   # BR(chi^0_4 -> chi^0_3 c c_bar)
     1.77601526E-03    3     1000025         1        -1   # BR(chi^0_4 -> chi^0_3 d d_bar)
     1.77599773E-03    3     1000025         3        -3   # BR(chi^0_4 -> chi^0_3 s s_bar)
     1.74199303E-03    3     1000025         5        -5   # BR(chi^0_4 -> chi^0_3 b b_bar)
     4.00729515E-04    3     1000025        11       -11   # BR(chi^0_4 -> chi^0_3 e^- e^+)
     4.00723350E-04    3     1000025        13       -13   # BR(chi^0_4 -> chi^0_3 mu^- mu^+)
     3.98798629E-04    3     1000025        15       -15   # BR(chi^0_4 -> chi^0_3 tau^- tau^+)
     2.37419471E-03    3     1000025        12       -12   # BR(chi^0_4 -> chi^0_3 nu_e nu_bar_e)
DECAY   1000021     3.19012409E+02   # ~g
#    BR                NDA      ID1      ID2
     3.98683664E-02    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     3.98683664E-02    2    -2000002         2   # BR(~g -> ~u^*_R u)
     3.94457643E-02    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     3.94457643E-02    2    -1000002         2   # BR(~g -> ~u^*_L u)
     3.98683548E-02    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     3.98683548E-02    2    -2000004         4   # BR(~g -> ~c^*_R c)
     3.94458248E-02    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     3.94458248E-02    2    -1000004         4   # BR(~g -> ~c^*_L c)
     1.19287266E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.19287266E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     6.32310855E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     6.32310855E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.50604532E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     2.50604532E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     3.98738934E-02    2     2000001        -1   # BR(~g -> ~d_R d_bar)
     3.98738934E-02    2    -2000001         1   # BR(~g -> ~d^*_R d)
     3.93058478E-02    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     3.93058478E-02    2    -1000001         1   # BR(~g -> ~d^*_L d)
     3.98763108E-02    2     2000003        -3   # BR(~g -> ~s_R s_bar)
     3.98763108E-02    2    -2000003         3   # BR(~g -> ~s^*_R s)
     3.93037159E-02    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     3.93037159E-02    2    -1000003         3   # BR(~g -> ~s^*_L s)
     4.28119537E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     4.28119537E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     3.99634665E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     3.99634665E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
DECAY        25     4.30987833E-03   # h^0
#    BR                NDA      ID1      ID2
     2.14512074E-04    2          13       -13   # BR(h^0 -> mu^- mu^+)
     5.96608537E-02    2          15       -15   # BR(h^0 -> tau^- tau^+)
     2.58893997E-04    2           3        -3   # BR(h^0 -> s s_bar)
     6.49832580E-01    2           5        -5   # BR(h^0 -> b b_bar)
     4.25873396E-02    2           4        -4   # BR(h^0 -> c c_bar)
     8.46202330E-02    2          21        21   # BR(h^0 -> g g)
     2.40495477E-03    2          22        22   # BR(h^0 -> photon photon)
# writing decays into V V* as 3-body decays
#    BR                NDA      ID1      ID2       ID3
     7.27857105E-03    3          24        11        12   # BR(h^0 -> W^+ e^- nu_e)
     7.27857105E-03    3          24        13        14   # BR(h^0 -> W^+ mu^- nu_mu)
     7.27857105E-03    3          24        15        16   # BR(h^0 -> W^+ tau^- nu_tau)
     2.54749987E-02    3          24         1        -2   # BR(h^0 -> W^+ d u_bar)
     2.54749987E-02    3          24         3        -4   # BR(h^0 -> W^+ s c_bar)
     7.27857105E-03    3         -24       -11       -12   # BR(h^0 -> W^- e^+ nu_bar_e)
     7.27857105E-03    3         -24       -13       -14   # BR(h^0 -> W^- mu^+ nu_bar_mu)
     7.27857105E-03    3         -24       -15       -16   # BR(h^0 -> W^- tau^+ nu_bar_tau)
     2.54749987E-02    3         -24        -1         2   # BR(h^0 -> W^- d_bar u)
     2.54749987E-02    3         -24        -3         4   # BR(h^0 -> W^- s_bar c)
     4.98903714E-04    3          23        11       -11   # BR(h^0 -> Z e^- e^+)
     4.98903714E-04    3          23        13       -13   # BR(h^0 -> Z mu^- mu^+)
     5.01873379E-04    3          23        15       -15   # BR(h^0 -> Z tau^- tau^+)
     2.96966496E-03    3          23        12       -12   # BR(h^0 -> Z nu_e nu_bar_e)
     2.31633867E-03    3          23         1        -1   # BR(h^0 -> Z d d_bar)
     2.31633867E-03    3          23         3        -3   # BR(h^0 -> Z s s_bar)
     2.24209705E-03    3          23         5        -5   # BR(h^0 -> Z b b_bar)
     1.72240568E-03    3          23         2        -2   # BR(h^0 -> Z u u_bar)
     1.78179898E-03    3          23         4        -4   # BR(h^0 -> Z c c_bar)
DECAY        35     2.86372399E+01   # H^0
#    BR                NDA      ID1      ID2
     3.49120018E-04    2          13       -13   # BR(H^0 -> mu^- mu^+)
     9.87484815E-02    2          15       -15   # BR(H^0 -> tau^- tau^+)
     3.30151295E-04    2           3        -3   # BR(H^0 -> s s_bar)
     8.99396828E-01    2           5        -5   # BR(H^0 -> b b_bar)
     1.10434163E-03    2           6        -6   # BR(H^0 -> t t_bar)
DECAY        36     2.86441284E+01   # A^0
#    BR                NDA      ID1      ID2
     3.49084144E-04    2          13       -13   # BR(A^0 -> mu^- mu^+)
     9.87400554E-02    2          15       -15   # BR(A^0 -> tau^- tau^+)
     3.30117757E-04    2           3        -3   # BR(A^0 -> s s_bar)
     8.99318279E-01    2           5        -5   # BR(A^0 -> b b_bar)
     1.23952055E-03    2           6        -6   # BR(A^0 -> t t_bar)
DECAY        37     2.38287543E+01   # H^+
#    BR                NDA      ID1      ID2
     4.21781715E-04    2         -13        12   # BR(H^+ -> mu^+ nu_e)
     1.19302908E-01    2         -15        12   # BR(H^+ -> tau^+ nu_e)
     3.50872402E-04    2          -3         4   # BR(H^+ -> s_bar c)
     8.79922724E-01    2          -5         6   # BR(H^+ -> b_bar t)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.02415655E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.59997584E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.60000000E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99984902E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.40097846E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.25000000E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.02415655E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.59997584E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.60000000E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999910E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    9.00937303E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999910E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    9.00937303E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.24828995E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.41571296E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.61791605E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    9.00937303E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999910E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.93960500E-04   # BR(b -> s gamma)
    2    1.58542365E-06   # BR(b -> s mu+ mu-)
    3    3.51946823E-05   # BR(b -> s nu nu)
    4    2.21571779E-15   # BR(Bd -> e+ e-)
    5    9.46359065E-11   # BR(Bd -> mu+ mu-)
    6    1.88200306E-08   # BR(Bd -> tau+ tau-)
    7    7.29309911E-14   # BR(Bs -> e+ e-)
    8    3.11498613E-09   # BR(Bs -> mu+ mu-)
    9    6.23977954E-07   # BR(Bs -> tau+ tau-)
   10    7.80996237E-05   # BR(B_u -> tau nu)
   11    8.06739213E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.43736173E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93538269E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16950549E-03   # epsilon_K
   17    2.28179787E-15   # Delta(M_K)
   18    2.47692829E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28131894E-11   # BR(K^+ -> pi^+ nu nu)
   20    5.40262843E-15   # Delta(g-2)_electron/2
   21    2.30975613E-10   # Delta(g-2)_muon/2
   22    6.52361691E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    9.06513735E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.65135192E-01   # C7
     0305 4322   00   2    -1.52437376E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.61659442E-01   # C8
     0305 6321   00   2    -1.31679686E-03   # C8'
 03051111 4133   00   0     1.59221611E+00   # C9 e+e-
 03051111 4133   00   2     1.59291423E+00   # C9 e+e-
 03051111 4233   00   2    -5.91392882E-05   # C9' e+e-
 03051111 4137   00   0    -4.41490821E+00   # C10 e+e-
 03051111 4137   00   2    -4.40752928E+00   # C10 e+e-
 03051111 4237   00   2     4.63540791E-04   # C10' e+e-
 03051313 4133   00   0     1.59221611E+00   # C9 mu+mu-
 03051313 4133   00   2     1.59291201E+00   # C9 mu+mu-
 03051313 4233   00   2    -5.91645648E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.41490821E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.40753150E+00   # C10 mu+mu-
 03051313 4237   00   2     4.63563576E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50374767E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -1.01017245E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50374767E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -1.01011881E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50374767E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -9.95004885E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85677907E-07   # C7
     0305 4422   00   2     3.76106214E-05   # C7
     0305 4322   00   2    -1.27326871E-05   # C7'
     0305 6421   00   0     3.30358634E-07   # C8
     0305 6421   00   2     1.13019889E-06   # C8
     0305 6321   00   2    -6.73920635E-06   # C8'
 03051111 4133   00   2    -3.31093121E-06   # C9 e+e-
 03051111 4233   00   2     3.49210764E-06   # C9' e+e-
 03051111 4137   00   2     2.86684406E-05   # C10 e+e-
 03051111 4237   00   2    -2.71683446E-05   # C10' e+e-
 03051313 4133   00   2    -3.31088640E-06   # C9 mu+mu-
 03051313 4233   00   2     3.49209702E-06   # C9' mu+mu-
 03051313 4137   00   2     2.86685000E-05   # C10 mu+mu-
 03051313 4237   00   2    -2.71683785E-05   # C10' mu+mu-
 03051212 4137   00   2    -6.13387036E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     5.92069877E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -6.13387046E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     5.92069877E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -6.13392304E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     5.92069878E-06   # C11' nu_3 nu_3
