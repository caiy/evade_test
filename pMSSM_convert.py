import numpy as np
import pandas as pd
import pyslha
import os

if __name__ == "__main__":
    
    path = './spheno_FH/'
    aux_dict = {}
    aux_dict["tanb"] = []
    aux_dict['mA'] = []
    model_dict = {}
    model_dict["MINPAR_3"]   = []
    model_dict["SMINPUTS_1"] = []
    model_dict["SMINPUTS_2"] = []
    model_dict["SMINPUTS_3"] = []
    model_dict["SMINPUTS_4"] = []
    model_dict["SMINPUTS_5"] = []
    model_dict["SMINPUTS_6"] = []
    model_dict["SMINPUTS_7"] = []
    model_dict["GAUGE_Q"]    = []
    model_dict["GAUGE_1"]    = []
    model_dict["GAUGE_2"]    = []
    model_dict["GAUGE_3"]    = []
    model_dict["HMIX_Q"]     = []
    model_dict["HMIX_1"]     = []
    model_dict["HMIX_2"]     = []
    model_dict["HMIX_3"]     = []
    model_dict["HMIX_4"]     = []
    model_dict["HMIX_10"]    = []
    model_dict["HMIX_101"]   = []
    model_dict["HMIX_102"]   = []
    model_dict["HMIX_103"]   = []
    model_dict["MSOFT_Q"]    = []
    model_dict["MSOFT_1"]    = []
    model_dict["MSOFT_2"]    = []
    model_dict["MSOFT_3"]    = []
    model_dict["MSOFT_21"]   = []
    model_dict["MSOFT_22"]   = []
    model_dict["YU_Q"]       = []
    model_dict["YU_1_1"]     = []
    model_dict["YU_1_2"]     = []
    model_dict["YU_1_3"]     = []
    model_dict["YU_2_1"]     = []
    model_dict["YU_2_2"]     = []
    model_dict["YU_2_3"]     = []
    model_dict["YU_3_1"]     = []
    model_dict["YU_3_2"]     = []
    model_dict["YU_3_3"]     = []
    model_dict["YD_Q"]       = []
    model_dict["YD_1_1"]     = []
    model_dict["YD_1_2"]     = []
    model_dict["YD_1_3"]     = []
    model_dict["YD_2_1"]     = []
    model_dict["YD_2_2"]     = []
    model_dict["YD_2_3"]     = []
    model_dict["YD_3_1"]     = []
    model_dict["YD_3_2"]     = []
    model_dict["YD_3_3"]     = []
    model_dict["YE_Q"]       = []
    model_dict["YE_1_1"]     = []
    model_dict["YE_1_2"]     = []
    model_dict["YE_1_3"]     = []
    model_dict["YE_2_1"]     = []
    model_dict["YE_2_2"]     = []
    model_dict["YE_2_3"]     = []
    model_dict["YE_3_1"]     = []
    model_dict["YE_3_2"]     = []
    model_dict["YE_3_3"]     = []
    model_dict["TU_Q"]       = []
    model_dict["TU_1_1"]     = []
    model_dict["TU_1_2"]     = []
    model_dict["TU_1_3"]     = []
    model_dict["TU_2_1"]     = []
    model_dict["TU_2_2"]     = []
    model_dict["TU_2_3"]     = []
    model_dict["TU_3_1"]     = []
    model_dict["TU_3_2"]     = []
    model_dict["TU_3_3"]     = []
    model_dict["TD_Q"]       = []
    model_dict["TD_1_1"]     = []
    model_dict["TD_1_2"]     = []
    model_dict["TD_1_3"]     = []
    model_dict["TD_2_1"]     = []
    model_dict["TD_2_2"]     = []
    model_dict["TD_2_3"]     = []
    model_dict["TD_3_1"]     = []
    model_dict["TD_3_2"]     = []
    model_dict["TD_3_3"]     = []
    model_dict["TE_Q"]       = []
    model_dict["TE_1_1"]     = []
    model_dict["TE_1_2"]     = []
    model_dict["TE_1_3"]     = []
    model_dict["TE_2_1"]     = []
    model_dict["TE_2_2"]     = []
    model_dict["TE_2_3"]     = []
    model_dict["TE_3_1"]     = []
    model_dict["TE_3_2"]     = []
    model_dict["TE_3_3"]     = []
    model_dict["MSQ2_Q"]     = []
    model_dict["MSQ2_1_1"]   = []
    model_dict["MSQ2_1_2"]   = []
    model_dict["MSQ2_1_3"]   = []
    model_dict["MSQ2_2_1"]   = []
    model_dict["MSQ2_2_2"]   = []
    model_dict["MSQ2_2_3"]   = []
    model_dict["MSQ2_3_1"]   = []
    model_dict["MSQ2_3_2"]   = []
    model_dict["MSQ2_3_3"]   = []
    model_dict["MSL2_Q"]     = []
    model_dict["MSL2_1_1"]   = []
    model_dict["MSL2_1_2"]   = []
    model_dict["MSL2_1_3"]   = []
    model_dict["MSL2_2_1"]   = []
    model_dict["MSL2_2_2"]   = []
    model_dict["MSL2_2_3"]   = []
    model_dict["MSL2_3_1"]   = []
    model_dict["MSL2_3_2"]   = []
    model_dict["MSL2_3_3"]   = []
    model_dict["MSD2_Q"]     = []
    model_dict["MSD2_1_1"]   = []
    model_dict["MSD2_1_2"]   = []
    model_dict["MSD2_1_3"]   = []
    model_dict["MSD2_2_1"]   = []
    model_dict["MSD2_2_2"]   = []
    model_dict["MSD2_2_3"]   = []
    model_dict["MSD2_3_1"]   = []
    model_dict["MSD2_3_2"]   = []
    model_dict["MSD2_3_3"]   = []
    model_dict["MSU2_Q"]     = []
    model_dict["MSU2_1_1"]   = []
    model_dict["MSU2_1_2"]   = []
    model_dict["MSU2_1_3"]   = []
    model_dict["MSU2_2_1"]   = []
    model_dict["MSU2_2_2"]   = []
    model_dict["MSU2_2_3"]   = []
    model_dict["MSU2_3_1"]   = []
    model_dict["MSU2_3_2"]   = []
    model_dict["MSU2_3_3"]   = []
    model_dict["MSE2_Q"]     = []
    model_dict["MSE2_1_1"]   = []
    model_dict["MSE2_1_2"]   = []
    model_dict["MSE2_1_3"]   = []
    model_dict["MSE2_2_1"]   = []
    model_dict["MSE2_2_2"]   = []
    model_dict["MSE2_2_3"]   = []
    model_dict["MSE2_3_1"]   = []
    model_dict["MSE2_3_2"]   = []
    model_dict["MSE2_3_3"]   = []
    for filename in os.listdir(path):
        filepath = os.path.join(path, filename)
        if os.path.isfile(filepath):
            try:
                d = pyslha.read(filepath)
            except:
                continue
            aux_dict["mA"  ].append( d.blocks["MASS"][36] )
            aux_dict["tanb"].append( d.blocks["MINPAR"][3] )
            model_dict["MINPAR_3"]   .append( d.blocks["MINPAR"][3] )
            model_dict["SMINPUTS_1"] .append( d.blocks["SMINPUTS"][1] )
            model_dict["SMINPUTS_2"] .append( d.blocks["SMINPUTS"][2] )
            model_dict["SMINPUTS_3"] .append( d.blocks["SMINPUTS"][3] )
            model_dict["SMINPUTS_4"] .append( d.blocks["SMINPUTS"][4] )
            model_dict["SMINPUTS_5"] .append( d.blocks["SMINPUTS"][5] )
            model_dict["SMINPUTS_6"] .append( d.blocks["SMINPUTS"][6] )
            model_dict["SMINPUTS_7"] .append( d.blocks["SMINPUTS"][7] )
            model_dict["GAUGE_Q"]    .append( d.blocks["GAUGE"].q )
            model_dict["GAUGE_1"]    .append( d.blocks["GAUGE"][1] )
            model_dict["GAUGE_2"]    .append( d.blocks["GAUGE"][2] )
            model_dict["GAUGE_3"]    .append( d.blocks["GAUGE"][3] )
            model_dict["HMIX_Q"]     .append( d.blocks["HMIX"].q   )
            model_dict["HMIX_1"]     .append( d.blocks["HMIX"][1]  )
            model_dict["HMIX_2"]     .append( d.blocks["HMIX"][2]  )
            model_dict["HMIX_3"]     .append( d.blocks["HMIX"][3]  )
            model_dict["HMIX_4"]     .append( d.blocks["HMIX"][4]  )
            model_dict["HMIX_10"]    .append( np.arctan(d.blocks["HMIX"][2]) ) 
            model_dict["HMIX_101"]   .append( d.blocks["HMIX"][4] * np.sin( np.arctan(d.blocks["HMIX"][2]) ) * np.cos( np.arctan(d.blocks["HMIX"][2]) ) )
            model_dict["HMIX_102"]   .append( np.cos( np.arctan(d.blocks["HMIX"][2]) ) * d.blocks["HMIX"][3]                                            )
            model_dict["HMIX_103"]   .append( np.sin( np.arctan(d.blocks["HMIX"][2]) ) * d.blocks["HMIX"][3]                                            )
            model_dict["MSOFT_Q"]    .append( d.blocks["MSOFT"].q                                                                                       )
            model_dict["MSOFT_1"]    .append( d.blocks["MSOFT"][1]                                                                                      )
            model_dict["MSOFT_2"]    .append( d.blocks["MSOFT"][2]                                                                                      )
            model_dict["MSOFT_3"]    .append( d.blocks["MSOFT"][3]                                                                                      )
            model_dict["MSOFT_21"]   .append( d.blocks["MSOFT"][21]                                                                                     )
            model_dict["MSOFT_22"]   .append( d.blocks["MSOFT"][22]                                                                                     )
            model_dict["YU_Q"]       .append( d.blocks["YU"].q                                                                                          )
            model_dict["YU_1_1"]     .append( d.blocks["YU"][1,1]                                                                                       )
            model_dict["YU_1_2"]     .append( 0 ) #d.blocks["YU"][1,2]
            model_dict["YU_1_3"]     .append( 0 ) #d.blocks["YU"][1,3]
            model_dict["YU_2_1"]     .append( 0 ) #d.blocks["YU"][2,1]
            model_dict["YU_2_2"]     .append( d.blocks["YU"][2,2] )
            model_dict["YU_2_3"]     .append( 0 ) #d.blocks["YU"][2,3]
            model_dict["YU_3_1"]     .append( 0 ) #d.blocks["YU"][3,1]
            model_dict["YU_3_2"]     .append( 0 ) #d.blocks["YU"][3,2]
            model_dict["YU_3_3"]     .append( d.blocks["YU"][3,3] )
            model_dict["YD_Q"]       .append( d.blocks["YD"].q    )
            model_dict["YD_1_1"]     .append( d.blocks["YD"][1,1] )
            model_dict["YD_1_2"]     .append( 0 ) #d.blocks["YD"][1,2]
            model_dict["YD_1_3"]     .append( 0 ) #d.blocks["YD"][1,3]
            model_dict["YD_2_1"]     .append( 0 ) #d.blocks["YD"][2,1]
            model_dict["YD_2_2"]     .append( d.blocks["YD"][2,2] )
            model_dict["YD_2_3"]     .append( 0 ) #d.blocks["YD"][2,3]
            model_dict["YD_3_1"]     .append( 0 ) #d.blocks["YD"][3,1]
            model_dict["YD_3_2"]     .append( 0 ) #d.blocks["YD"][3,2]
            model_dict["YD_3_3"]     .append( d.blocks["YD"][3,3] )
            model_dict["YE_Q"]       .append( d.blocks["YE"].q    )
            model_dict["YE_1_1"]     .append( d.blocks["YE"][1,1] )
            model_dict["YE_1_2"]     .append( 0 ) #d.blocks["YE"][1,2]
            model_dict["YE_1_3"]     .append( 0 ) #d.blocks["YE"][1,3]
            model_dict["YE_2_1"]     .append( 0 ) #d.blocks["YE"][2,1]
            model_dict["YE_2_2"]     .append( d.blocks["YE"][2,2] )
            model_dict["YE_2_3"]     .append( 0 ) #d.blocks["YE"][2,3]
            model_dict["YE_3_1"]     .append( 0 ) #d.blocks["YE"][3,1]
            model_dict["YE_3_2"]     .append( 0 ) #d.blocks["YE"][3,2]
            model_dict["YE_3_3"]     .append( d.blocks["YE"][3,3] )
            model_dict["TU_Q"]       .append( d.blocks["AU"].q    )
            model_dict["TU_1_1"]     .append( d.blocks["AU"][1,1] )
            model_dict["TU_1_2"]     .append( 0 ) #d.blocks["AU"][1,2]
            model_dict["TU_1_3"]     .append( 0 ) #d.blocks["AU"][1,3]
            model_dict["TU_2_1"]     .append( 0 ) #d.blocks["AU"][2,1]
            model_dict["TU_2_2"]     .append( d.blocks["AU"][2,2] )
            model_dict["TU_2_3"]     .append( 0 ) #d.blocks["AU"][2,3]
            model_dict["TU_3_1"]     .append( 0 ) #d.blocks["AU"][3,1]
            model_dict["TU_3_2"]     .append( 0 ) #d.blocks["AU"][3,2]
            model_dict["TU_3_3"]     .append( d.blocks["AU"][3,3] )
            model_dict["TD_Q"]       .append( d.blocks["AD"].q    )
            model_dict["TD_1_1"]     .append( d.blocks["AD"][1,1] )
            model_dict["TD_1_2"]     .append( 0 ) #d.blocks["AD"][1,2]
            model_dict["TD_1_3"]     .append( 0 ) #d.blocks["AD"][1,3]
            model_dict["TD_2_1"]     .append( 0 ) #d.blocks["AD"][2,1]
            model_dict["TD_2_2"]     .append( d.blocks["AD"][2,2] )
            model_dict["TD_2_3"]     .append( 0 ) #d.blocks["AD"][2,3]
            model_dict["TD_3_1"]     .append( 0 ) #d.blocks["AD"][3,1]
            model_dict["TD_3_2"]     .append( 0 ) #d.blocks["AD"][3,2]
            model_dict["TD_3_3"]     .append( d.blocks["AD"][3,3] )
            model_dict["TE_Q"]       .append( d.blocks["AE"].q    )
            model_dict["TE_1_1"]     .append( d.blocks["AE"][1,1] )
            model_dict["TE_1_2"]     .append( 0 ) #d.blocks["AE"][1,2]     
            model_dict["TE_1_3"]     .append( 0 ) #d.blocks["AE"][1,3]     
            model_dict["TE_2_1"]     .append( 0 ) #d.blocks["AE"][2,1]     
            model_dict["TE_2_2"]     .append( d.blocks["AE"][2,2] )
            model_dict["TE_2_3"]     .append( 0 ) #d.blocks["AE"][2,3]
            model_dict["TE_3_1"]     .append( 0 ) #d.blocks["AE"][3,1]
            model_dict["TE_3_2"]     .append( 0 ) #d.blocks["AE"][3,2]
            model_dict["TE_3_3"]     .append( d.blocks["AE"][3,3] )
            model_dict["MSQ2_Q"]     .append( d.blocks["YU"].q ) #d.blocks["MSQ2"]["Q"]
            model_dict["MSQ2_1_1"]   .append( d.blocks["MSOFT"][41]*d.blocks["MSOFT"][41] ) #d.blocks["MSQ2"]["1_1"]
            model_dict["MSQ2_1_2"]   .append( 0 ) #d.blocks["MSQ2"]["1_2"]
            model_dict["MSQ2_1_3"]   .append( 0 ) #d.blocks["MSQ2"]["1_3"]
            model_dict["MSQ2_2_1"]   .append( 0 ) #d.blocks["MSQ2"]["2_1"]
            model_dict["MSQ2_2_2"]   .append( d.blocks["MSOFT"][42]*d.blocks["MSOFT"][42] ) #d.blocks["MSQ2"]["2_2"]
            model_dict["MSQ2_2_3"]   .append( 0 ) #d.blocks["MSQ2"]["2_3"]
            model_dict["MSQ2_3_1"]   .append( 0 ) #d.blocks["MSQ2"]["3_1"]
            model_dict["MSQ2_3_2"]   .append( 0 ) #d.blocks["MSQ2"]["3_2"]
            model_dict["MSQ2_3_3"]   .append( d.blocks["MSOFT"][43]*d.blocks["MSOFT"][43] ) #d.blocks["MSQ2"]["3_3"]
            model_dict["MSL2_Q"]     .append( d.blocks["YU"].q ) #d.blocks["MSL2"]["Q"]
            model_dict["MSL2_1_1"]   .append( d.blocks["MSOFT"][31]*d.blocks["MSOFT"][31] ) #d.blocks["MSL2"]["1_1"]
            model_dict["MSL2_1_2"]   .append( 0 ) #d.blocks["MSL2"]["1_2"]
            model_dict["MSL2_1_3"]   .append( 0 ) #d.blocks["MSL2"]["1_3"]
            model_dict["MSL2_2_1"]   .append( 0 ) #d.blocks["MSL2"]["2_1"]
            model_dict["MSL2_2_2"]   .append( d.blocks["MSOFT"][32]*d.blocks["MSOFT"][32] ) #d.blocks["MSL2"]["2_2"]
            model_dict["MSL2_2_3"]   .append( 0 ) #d.blocks["MSL2"]["2_3"]
            model_dict["MSL2_3_1"]   .append( 0 ) #d.blocks["MSL2"]["3_1"]
            model_dict["MSL2_3_2"]   .append( 0 ) #d.blocks["MSL2"]["3_2"]
            model_dict["MSL2_3_3"]   .append( d.blocks["MSOFT"][33]*d.blocks["MSOFT"][33] ) #d.blocks["MSL2"]["3_3"]
            model_dict["MSD2_Q"]     .append( d.blocks["YU"].q ) #d.blocks["MSD2"]["Q"]
            model_dict["MSD2_1_1"]   .append( d.blocks["MSOFT"][47]*d.blocks["MSOFT"][47] ) #d.blocks["MSD2"]["1_1"]
            model_dict["MSD2_1_2"]   .append( 0 ) #d.blocks["MSD2"]["1_2"]
            model_dict["MSD2_1_3"]   .append( 0 ) #d.blocks["MSD2"]["1_3"]
            model_dict["MSD2_2_1"]   .append( 0 ) #d.blocks["MSD2"]["2_1"]
            model_dict["MSD2_2_2"]   .append( d.blocks["MSOFT"][48]*d.blocks["MSOFT"][48] ) #d.blocks["MSD2"]["2_2"]
            model_dict["MSD2_2_3"]   .append( 0 ) #d.blocks["MSD2"]["2_3"]
            model_dict["MSD2_3_1"]   .append( 0 ) #d.blocks["MSD2"]["3_1"]
            model_dict["MSD2_3_2"]   .append( 0 ) #d.blocks["MSD2"]["3_2"]
            model_dict["MSD2_3_3"]   .append( d.blocks["MSOFT"][49]*d.blocks["MSOFT"][49] ) #d.blocks["MSD2"]["3_3"]
            model_dict["MSU2_Q"]     .append( d.blocks["YU"].q ) #d.blocks["MSU2"]["Q"]
            model_dict["MSU2_1_1"]   .append( d.blocks["MSOFT"][44]*d.blocks["MSOFT"][44] ) #d.blocks["MSU2"]["1_1"]
            model_dict["MSU2_1_2"]   .append( 0 ) #d.blocks["MSU2"]["1_2"]
            model_dict["MSU2_1_3"]   .append( 0 ) #d.blocks["MSU2"]["1_3"]
            model_dict["MSU2_2_1"]   .append( 0 ) #d.blocks["MSU2"]["2_1"]
            model_dict["MSU2_2_2"]   .append( d.blocks["MSOFT"][45]*d.blocks["MSOFT"][45] ) #d.blocks["MSU2"]["2_2"]
            model_dict["MSU2_2_3"]   .append( 0 ) #d.blocks["MSU2"]["2_3"]
            model_dict["MSU2_3_1"]   .append( 0 ) #d.blocks["MSU2"]["3_1"]
            model_dict["MSU2_3_2"]   .append( 0 ) #d.blocks["MSU2"]["3_2"]
            model_dict["MSU2_3_3"]   .append( d.blocks["MSOFT"][46]*d.blocks["MSOFT"][46] ) #d.blocks["MSU2"]["3_3"]
            model_dict["MSE2_Q"]     .append( d.blocks["YU"].q ) #d.blocks["MSE2"]["Q"]
            model_dict["MSE2_1_1"]   .append( d.blocks["MSOFT"][34]*d.blocks["MSOFT"][34] ) #d.blocks["MSE2"]["1_1"]
            model_dict["MSE2_1_2"]   .append( 0 ) #d.blocks["MSE2"]["1_2"]
            model_dict["MSE2_1_3"]   .append( 0 ) #d.blocks["MSE2"]["1_3"]
            model_dict["MSE2_2_1"]   .append( 0 ) #d.blocks["MSE2"]["2_1"]
            model_dict["MSE2_2_2"]   .append( d.blocks["MSOFT"][35]*d.blocks["MSOFT"][35] ) #d.blocks["MSE2"]["2_2"]
            model_dict["MSE2_2_3"]   .append( 0 ) #d.blocks["MSE2"]["2_3"]
            model_dict["MSE2_3_1"]   .append( 0 ) #d.blocks["MSE2"]["3_1"]
            model_dict["MSE2_3_2"]   .append( 0 ) #d.blocks["MSE2"]["3_2"]
            model_dict["MSE2_3_3"]   .append( d.blocks["MSOFT"][36]*d.blocks["MSOFT"][36] ) #d.blocks["MSE2"]["3_3"]
    model_df = pd.DataFrame(model_dict, columns=model_dict.keys() )
    aux_df = pd.DataFrame(aux_dict, columns=aux_dict.keys() )
    model_df.to_csv('localrun.in', sep ='\t')
    aux_df.to_csv('aux.save', sep ='\t')

